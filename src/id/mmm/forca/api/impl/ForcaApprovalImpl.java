package id.mmm.forca.api.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import org.compiere.model.MBPartner;
import org.compiere.model.MColumn;
import org.compiere.model.MCurrency;
import org.compiere.model.MInvoice;
import org.compiere.model.MOrder;
import org.compiere.model.MPayment;
import org.compiere.model.MRequisition;
import org.compiere.model.MTable;
import org.compiere.model.MUser;
import org.compiere.model.PO;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.DisplayType;
import org.compiere.util.Env;
import org.compiere.util.Msg;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.wf.MWFActivity;
import org.compiere.wf.MWFEventAudit;
import org.compiere.wf.MWFNode;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.sisi.forca.authenticationapi.response.ResponseData;

public class ForcaApprovalImpl {
    AuthLoginImpl authLoginImpl = new AuthLoginImpl();
    private CLogger log = CLogger.getCLogger(getClass());
    public Properties wsenv = new Properties();
    // private ArrayList<MWFNode> listCategory;

    public Properties getDefaultCtx() {
        wsenv.setProperty("#AD_Language", "en_US");
        wsenv.setProperty("context", "deverp.semenindonesia.com/ws");
        return wsenv;
    }

    public ResponseData getWorkflow(ParamRequest param, String tipe) {

        ResponseData resp = new ResponseData();
        Map<String, Object> mToken =
                (Map<String, Object>) authLoginImpl.infoToken(param).getResultdata();
        Integer ad_client_id = (Integer) mToken.get("ad_client_id");
        Integer ad_user_id = (Integer) mToken.get("ad_user_id");
        // MWFProcess wf_process = null;
        // MWFActivity wf_activity = null;
        MWFNode wf_node = null;
        String ad_wf_name = "";
        String ad_wf_node_id = "";
        String ad_wf_activity_id = "";
        String sql = "";
        String inWflow = "";

        if (!DB.isConnected()) {
            resp.setCodestatus("E");
            resp.setMessage("No Database Connection");
            return resp;
        }
        if (ad_client_id == null || ad_user_id == null) {
            resp.setCodestatus("E");
            resp.setMessage("Parameter required");
            return resp;
        }
        if (tipe == null || tipe.isEmpty()) {
            tipe = "";
        }

        if (tipe.equals("List")) {
            ad_wf_name = param.getAd_wf_name();
            ad_wf_node_id = param.getAd_wf_node_id();
        } else if (tipe.equals("Detail")) {
            ad_wf_name = param.getAd_wf_name();
            ad_wf_activity_id = param.getAd_wf_activity_id();
        }

        getDefaultCtx();
        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        // wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));

        if (tipe.equals("Group")) {
            sql = getSqlAllWflow();// getSqlGrupWflow();
        } else if (tipe.equals("List")) {
            sql = getSqlByWfName(ad_wf_name, ad_wf_node_id);
        } else if (tipe.equals("Detail")) {
            sql = getSqlWfDetail(ad_wf_name, ad_wf_activity_id);
        } else {
            sql = getSqlAllWflow();
        }

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = DB.prepareStatement(sql, null);
            pstmt.setInt(1, ad_user_id);
            pstmt.setInt(2, ad_user_id);
            pstmt.setInt(3, ad_user_id);
            pstmt.setInt(4, ad_user_id);
            pstmt.setInt(5, ad_client_id);
            rs = pstmt.executeQuery();

            // define
            List<Object> resultdata = new ArrayList<Object>();

            if (tipe.equals("Group")) {
                int z = 0;
                while (rs.next()) {
                    MWFActivity activity =
                            new MWFActivity(Env.getCtx(), rs.getInt("ad_wf_activity_id"), null);
                    if (activity.getPO() != null) {
                        inWflow += (z > 0) ? "," : "";
                        inWflow += rs.getInt("ad_wf_activity_id");
                        z++;
                    }
                }
                if (!rs.wasNull()) {
                    resultdata = getSqlGrupWflow(ad_client_id, ad_user_id, inWflow);
                }
            } else {
                while (rs.next()) {
                    MWFActivity activity =
                            new MWFActivity(Env.getCtx(), rs.getInt("ad_wf_activity_id"), null);
                    if (activity.getPO() != null) {
                        Map<String, String> map = new LinkedHashMap<String, String>();

                        map.put("ad_wf_activity_id", rs.getString("ad_wf_activity_id"));
                        map.put("wfname", rs.getString("wfname"));
                        map.put("nodename", rs.getString("nodename"));
                        map.put("record_id", rs.getString("record_id"));
                        map.put("wfstate", rs.getString("wfstate"));
                        map.put("ad_wf_node_id", rs.getString("ad_wf_node_id"));
                        map.put("ad_wf_process_id", rs.getString("ad_wf_process_id"));
                        map.put("approve_dtl", rs.getString("approve_dtl"));
                        map.put("created", rs.getString("created"));
                        map.put("detail1", getWfSummary(activity.getPO()));
                        map.put("nominal", getDescDetail(activity.getPO(), "price"));
                        map.put("detail3", getDescDetail(activity.getPO(), "description"));

                        if (tipe.equals("List") || tipe.equals("Detail")) {
                            if (!Util.isEmpty(rs.getString("ad_table_id"))
                                    && !Util.isEmpty(rs.getString("record_id"))) {
                                List<String> listDocData = getDataByTableNameRecord(
                                        rs.getString("ad_table_id"), rs.getString("record_id"));
                                map.put("description", listDocData.get(0));
                                map.put("doc_type", listDocData.get(1));
                                map.put("doc_number", listDocData.get(2));
                                map.put("bpartner_name", listDocData.get(3));
                                map.put("doc_date", listDocData.get(4));
                                map.put("grandtotal", listDocData.get(5));
                            }
                        }



                        // wf_process = new MWFProcess(Env.getCtx(), rs.getInt("ad_wf_process_id"),
                        // null);
                        // wf_activity = new MWFActivity(Env.getCtx(),
                        // rs.getInt("ad_wf_process_id"), null);
                        // wf_activity = new MWFActivity(wf_process, rs.getInt("ad_wf_node_id"));
                        // wf_node = wf_activity.getNode();
                        wf_node = new MWFNode(Env.getCtx(), rs.getInt("ad_wf_node_id"), null);


                        if (!MWFNode.ACTION_UserChoice.equals(wf_node.getAction())) {
                            map.put("yesno", "N");
                        } else {
                            map.put("yesno", "Y");
                        }
                        if (tipe.equals("Detail")) {
                            map.put("history", detailHistory(activity.getAD_WF_Process_ID()));
                            map.put("param_Ad_wf_name", param.getAd_wf_name());
                            map.put("param_Ad_wf_activity_id", param.getAd_wf_activity_id());
                            resp.setResultdata(map);
                            resultdata.add(map);
                        } else {
                            resultdata.add(map);
                        }
                    }
                }
            }
            resp.setCodestatus("S");
            resp.setMessage(resultdata.size() + " Data Founded");
            if (!tipe.equals("Detail")) {
                resp.setResultdata(resultdata);
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, sql, e);
            resp.setCodestatus("E");
            resp.setMessage("Parameter required");
            // return resp;
        } finally {
            DB.close(rs, pstmt);
            rs = null;
            pstmt = null;
        }
        return resp;
    }

    public ResponseData approveDokumen(ParamRequest param) {
        ResponseData resp = new ResponseData();
        Map<String, Object> mToken =
                (Map<String, Object>) authLoginImpl.infoToken(param).getResultdata();
        Integer ad_client_id = (Integer) mToken.get("ad_client_id");
        Integer ad_user_id = (Integer) mToken.get("ad_user_id");
        // String ad_wf_name = param.getAd_wf_name();
        String ad_wf_activity_id = param.getAd_wf_activity_id();
        String message = param.getMessage();
        String value = param.getYesno();
        // boolean sukses = true;
        // Boolean user_choice = true;

        getDefaultCtx();// wsenv
        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        Env.setCtx(wsenv);
        Trx trx = Trx.get(Trx.createTrxName("FWFA"), true);

        MWFActivity m_activity =
                new MWFActivity(Env.getCtx(), Integer.valueOf(ad_wf_activity_id), trx.getTrxName());
        // m_activity.set_TrxName();
        int dt = DisplayType.YesNo;
        // String message = "Dokumen Approved";//Message dari user
        // String value = "Y";

        try {
            m_activity.setUserChoice(ad_user_id, value, dt, message);
            trx.commit();
            trx.close();
            resp.setCodestatus("S");
            if (value.equals("Y"))
                resp.setMessage("Success Approve Document");
            else {
                resp.setMessage("Success Reject Document");
            }
        } catch (Exception e) {
            trx.rollback();
            trx.close();
            resp.setCodestatus("E");
            resp.setMessage("Failed Execution");
        }

        return resp;
    }

    public ResponseData approveMultiDokumen(ParamRequest param) {
        ResponseData resp = new ResponseData();
        Map<String, Object> mToken =
                (Map<String, Object>) authLoginImpl.infoToken(param).getResultdata();
        Integer ad_client_id = (Integer) mToken.get("ad_client_id");
        Integer ad_user_id = (Integer) mToken.get("ad_user_id");
        String ad_wf_activity_id = param.getAd_wf_activity_id();
        String message = param.getMessage();
        String value = param.getYesno().toUpperCase();
        int dt = DisplayType.YesNo;

        getDefaultCtx();// wsenv
        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        Env.setCtx(wsenv);
        Trx trx = null;

        try {
            String[] split = ad_wf_activity_id.split("/");
            for (int i = 0; i < split.length; i++) {
                trx = Trx.get(Trx.createTrxName("FWFA"), true);

                MWFActivity m_activity =
                        new MWFActivity(Env.getCtx(), Integer.valueOf(split[i]), trx.getTrxName());
                m_activity.setUserChoice(ad_user_id, value, dt, message);
                trx.commit();
                trx.close();
                // System.out.println(split[i]);
            }

            resp.setCodestatus("S");
            if (value.equals("Y"))
                resp.setMessage("Document has been Approved");
            else {
                resp.setMessage("Document Has Been Rejected");
            }
        } catch (Exception e) {
            trx.rollback();
            trx.close();
            resp.setCodestatus("E");
            resp.setMessage("Failed Execution Caused By " + e.getMessage());
        }

        return resp;
    }

    private String getSqlWorkflow(String customWhere) {

        if (customWhere == null || customWhere.isEmpty()) {
            customWhere = "";
        }

        final String where = "a.Processed='N' AND a.WFState='OS' AND ("
                // Owner of Activity
                + " a.AD_User_ID=?" // #1
                // Invoker (if no invoker = all)
                + " OR EXISTS (SELECT * FROM AD_WF_Responsible r WHERE a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID"
                + " AND r.ResponsibleType='H' AND COALESCE(r.AD_User_ID,0)=0 AND COALESCE(r.AD_Role_ID,0)=0 AND (a.AD_User_ID=? OR a.AD_User_ID IS NULL))" // #2
                // Responsible User
                + " OR EXISTS (SELECT * FROM AD_WF_Responsible r WHERE a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID"
                + " AND r.ResponsibleType='H' AND r.AD_User_ID=?)" // #3
                // Responsible Role
                + " OR EXISTS (SELECT * FROM AD_WF_Responsible r INNER JOIN AD_User_Roles ur ON (r.AD_Role_ID=ur.AD_Role_ID)"
                + " WHERE a.AD_WF_Responsible_ID=r.AD_WF_Responsible_ID AND r.ResponsibleType='R' AND ur.AD_User_ID=?)" // #4
                //
                + ") AND a.AD_Client_ID=?"; // #5

        // String xsql =
        // " Select a.ad_wf_activity_id, b.name as wfname,b.VALUE AS wfdesc, a.ad_client_id,
        // a.ad_org_id, a.isactive, a.created,a.createdby, a.updated, a.updatedby, "
        // + " a.ad_wf_process_id, a.ad_wf_node_id, a.ad_wf_responsible_id, a.ad_user_id, a.wfstate,
        // a.ad_message_id, a.processing, a.processed, "
        // + " a.textmsg, a.ad_workflow_id, a.ad_table_id, a.record_id, a.priority, a.endwaittime,
        // a.datelastalert, a.dynprioritystart, a.ad_wf_activity_uu, "
        // + " C.NAME as nodename, D.textmsg as approve_dtl"
        // + " FROM AD_WF_Activity A left JOIN ad_workflow b on A.ad_workflow_id = b.ad_workflow_id"
        // + " LEFT JOIN ad_wf_node C ON A.ad_wf_node_id = C.ad_wf_node_id"
        // + " LEFT JOIN ad_wf_process D ON A.ad_wf_process_id = D.ad_wf_process_id "
        // + " WHERE " + where + customWhere + " ORDER BY a.Priority DESC, a.created";

        String xsql = "select " + "    a.ad_wf_activity_id, " + "    b.name as wfname, "
                + "    b.VALUE as wfdesc, " + "    a.ad_client_id, " + "    a.ad_org_id, "
                + "    a.isactive, " + "    a.created, " + "    a.createdby, " + "    a.updated, "
                + "    a.updatedby, " + "    a.ad_wf_process_id, " + "    a.ad_wf_node_id, "
                + "    a.ad_wf_responsible_id, " + "    a.ad_user_id, " + "    a.wfstate, "
                + "    a.ad_message_id, " + "    a.processing, " + "    a.processed, "
                + "    a.textmsg, " + "    a.ad_workflow_id, " + "    a.ad_table_id, "
                + "    a.record_id, " + "    a.priority, " + "    a.endwaittime, "
                + "    a.datelastalert, " + "    a.dynprioritystart, " + "    a.ad_wf_activity_uu, "
                + "    C.NAME as nodename, " + "    D.textmsg as approve_dtl " + "from "
                + "    AD_WF_Activity A " + "left join ad_workflow b on "
                + "    A.ad_workflow_id = b.ad_workflow_id " + "left join ad_wf_node C on "
                + "    A.ad_wf_node_id = C.ad_wf_node_id " + "left join ad_wf_process D on "
                + "    A.ad_wf_process_id = D.ad_wf_process_id " + "where " + where + customWhere
                + "order by " + "    a.Priority desc, " + "    a.created ";
        return xsql;
    }

    // Function SQl Workflowne
    private String getSqlAllWflow() {
        String psql = getSqlWorkflow(null);
        return psql;
    }

    private List<Object> getSqlGrupWflow(Integer ad_client_id, Integer ad_user_id, String inwflow) {
        List<Object> result = new ArrayList<Object>();

        String inwhere = " and a.ad_wf_activity_id in (" + inwflow + ") ";
        String psql =
                " Select wfname,nodename,wfdesc,ad_workflow_id,ad_wf_node_id, count(*) as total "
                        + " FROM( " + getSqlWorkflow(inwhere)
                        + " ) a group by wfname,wfdesc,ad_workflow_id,nodename,ad_wf_node_id ";

        PreparedStatement pstmtx = null;
        ResultSet rsx = null;
        try {
            pstmtx = DB.prepareStatement(psql, null);
            pstmtx.setInt(1, ad_user_id);
            pstmtx.setInt(2, ad_user_id);
            pstmtx.setInt(3, ad_user_id);
            pstmtx.setInt(4, ad_user_id);
            pstmtx.setInt(5, ad_client_id);
            rsx = pstmtx.executeQuery();
            while (rsx.next()) {
                Map<String, String> map = new LinkedHashMap<String, String>();

                map.put("wfname", rsx.getString("wfname"));
                map.put("nodename", rsx.getString("nodename"));
                map.put("wfdesc", rsx.getString("wfdesc"));
                map.put("ad_workflow_id", rsx.getString("ad_workflow_id"));
                map.put("wfnode", rsx.getString("ad_wf_node_id"));
                map.put("total", rsx.getString("total"));
                result.add(map);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, psql, e);
        } finally {
            DB.close(rsx, pstmtx);
            rsx = null;
            pstmtx = null;
        }
        return result;
    }

    private String getSqlByWfName(String wfName, String wfnodeid) {
        String where = " and B.name = '" + wfName + "' and c.ad_wf_node_id = '" + wfnodeid + "' ";
        String psql = getSqlWorkflow(where);
        return psql;
    }

    private String getSqlWfDetail(String wfName, String wfID) {
        String where = " and B.name = '" + wfName + "' and a.ad_wf_activity_id = " + wfID + " ";
        String psql = getSqlWorkflow(where);
        return psql;
    }



    public String toRupiahFormat(String nominal) {
        String rupiah = "";
        if (!nominal.equals("")) {
            Locale locale = new Locale("ca", "CA");
            NumberFormat rupiahFormat = NumberFormat.getCurrencyInstance(locale);

            rupiah = rupiahFormat.format(Double.parseDouble(nominal)).substring(4);
        }
        return rupiah;
    }


    // Dokumen Approved / setelah di approve
    public ResponseData DokumenApproved(ParamRequest param, String path) {
        ResponseData resp = new ResponseData();

        SimpleDateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat frmformat = new SimpleDateFormat("dd/MM/yyyy");
        Map<String, Object> mToken =
                (Map<String, Object>) authLoginImpl.infoToken(param).getResultdata();
        Integer ad_client_id = (Integer) mToken.get("ad_client_id");
        Integer ad_user_id = (Integer) mToken.get("ad_user_id");
        String stglfrom = param.getXtglfrom();
        String stglto = param.getXtglto();
        String statusParam = param.getStatus();
        String ad_wf_activity_id = param.getAd_wf_activity_id();
        Date xTglFrom = null;
        Date xTglTo = null;

        String status = "CC";
        String whereCond = "";
        if (path.equals("Detail")) {
            whereCond += " and wf.ad_wf_activity_id = ? ";
        } else {
            if (statusParam.equals("Reject")) {
                status = "CA";
            }
            try {
                xTglFrom = frmformat.parse(stglfrom);
                xTglTo = frmformat.parse(stglto);
            } catch (ParseException e) {
                resp.setCodestatus("E");
                resp.setMessage("Parameter invalid date format");
                e.printStackTrace();
            }
            whereCond += " and to_char(wf.updated,'YYYY-MM-DD') BETWEEN ? and ? "
                    + " and wf.wfstate = ?::bpchar AND wn.action = 'C'::bpchar ";
        }

        //@formatter:off
        String xsql = 
                "SELECT " +
                "           wf.ad_wf_activity_id, " + 
                "           wf.ad_client_id, " + 
                "           wf.ad_org_id, " + 
                "           w.name AS wfname, " + 
                "           wf.wfstate, " + 
                "           case " + 
                "               when wf.wfstate = 'CC' then 'Accept' " + 
                "               else 'Reject' " + 
                "           end wfstate_name, " +
                "           wn.name AS nodename, " + 
                "           usr.ad_user_id AS responsibleid, " + 
                "           usr.name AS responsiblename, " + 
                "           wf.textmsg, " + 
                "           usr2.ad_user_id AS fromid, " + 
                "           usr2.name AS fromname, " + 
                "           trunc(wf.created::timestamp with time zone) AS created, " + 
                "           trunc(wf.updated::timestamp with time zone) AS updated, " + 
                "           wf.ad_table_id,  " + 
                "           wf.record_id " + 
                "   FROM ad_wf_activity wf " + 
                "     JOIN ad_wf_process wp ON wp.ad_wf_process_id = wf.ad_wf_process_id " + 
                "     JOIN ad_workflow w ON w.ad_workflow_id = wf.ad_workflow_id " + 
                "     JOIN ad_wf_node wn ON wn.ad_wf_node_id = wf.ad_wf_node_id " + 
                "     JOIN ad_user usr ON usr.ad_user_id = wf.ad_user_id " + 
                "     JOIN ad_user usr2 ON usr2.ad_user_id = wp.ad_user_id " + 
                "     INNER JOIN ad_user rsp on usr.name = rsp.Name " + 
                "  WHERE usr.AD_User_ID =? and usr.ad_client_id = ? " + 
                whereCond+
                "  ORDER BY wf.ad_workflow_id, wf.record_id";
      //@formatter:on
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = DB.prepareStatement(xsql, null);

            pstmt.setInt(1, ad_user_id);
            pstmt.setInt(2, ad_client_id);
            if (path.equals("Detail")) {
                pstmt.setInt(3, Integer.valueOf(ad_wf_activity_id));
            } else {
                pstmt.setString(3, dtFormat.format(xTglFrom));
                pstmt.setString(4, dtFormat.format(xTglTo));
                pstmt.setString(5, status);
            }
            rs = pstmt.executeQuery();

            // define
            List<Object> resultdata = new ArrayList<Object>();
            int n = 0;
            while (rs.next()) {
                MWFActivity activity =
                        new MWFActivity(Env.getCtx(), rs.getInt("ad_wf_activity_id"), null);
                if (activity.getPO() != null) {
                    Map<String, Object> map = new LinkedHashMap<String, Object>();
                    map.put("ad_wf_activity_id", rs.getString("ad_wf_activity_id"));
                    map.put("wfname", rs.getString("wfname"));
                    map.put("nodename", rs.getString("nodename"));
                    map.put("fromname", rs.getString("fromname"));
                    map.put("wfstate_name", rs.getString("wfstate_name"));
                    map.put("textmsg", rs.getString("textmsg"));
                    map.put("updated", rs.getString("updated"));
                    map.put("detail1", activity.getSummary());
                    map.put("nominal", getDescDetail(activity.getPO(), "price"));
                    map.put("detail3", getDescDetail(activity.getPO(), "description"));

                    if (!Util.isEmpty(rs.getString("ad_table_id"))
                            && !Util.isEmpty(rs.getString("record_id"))) {
                        List<String> listDocData = getDataByTableNameRecord(
                                rs.getString("ad_table_id"), rs.getString("record_id"));
                        map.put("description", listDocData.get(0));
                        map.put("doc_type", listDocData.get(1));
                        map.put("doc_number", listDocData.get(2));
                        map.put("bpartner_name", listDocData.get(3));
                        map.put("doc_date", listDocData.get(4));
                        map.put("grandtotal", listDocData.get(5));
                    }

                    if (path.equals("Detail")) {
                        map.put("history", detailHistory(activity.getAD_WF_Process_ID()));
                        resp.setResultdata(map);
                    }
                    resultdata.add(map);
                    n++;
                }
            }
            resp.setCodestatus("S");
            resp.setMessage("Data found, total = " + n);
            if (path.equals("Header")) {
                resp.setResultdata(resultdata);
            }

        } catch (Exception e) {
            log.log(Level.SEVERE, xsql, e);
            resp.setCodestatus("E");
            resp.setMessage("Parameter required");
            // return resp;
        } finally {
            DB.close(rs, pstmt);
            rs = null;
            pstmt = null;
        }
        return resp;
    }

    public String detailHistory(int WfProsesid) {
        // SimpleDateFormat format = DisplayType.getDateFormat(DisplayType.DateTime);
        StringBuilder sb = new StringBuilder();
        MWFEventAudit[] events = MWFEventAudit.get(Env.getCtx(), WfProsesid, null);
        for (int i = 0; i < events.length; i++) {
            MWFEventAudit audit = events[i];
            if (audit.getWFState().equals("CC")) {
                // MWFResponsible wfrespon = new MWFResponsible(Env.getCtx(),
                // audit.getAD_WF_Responsible_ID(), null);
                MUser usere = new MUser(Env.getCtx(), audit.getAD_User_ID(), null);

                String tgle = audit.getCreated().toString();
                sb.append("");
                sb.append(tgle.substring(0, 16)).append(" ")
                        // .append(audit.getNodeName())
                        .append(":")
                        // .append(" "+ audit.getDescription());
                        .append("" + usere.getName());
                // .append(" "+ audit.getTextMsg());
                sb.append("<br/>");
            }
        }

        return sb.toString();
    }

    public String getWfSummary(PO po) {
        // PO po = getPO();
        if (po == null)
            return null;
        StringBuilder sb = new StringBuilder();
        String[] keyColumns = po.get_KeyColumns();
        if ((keyColumns != null) && (keyColumns.length > 0))
            sb.append(Msg.getElement(Env.getCtx(), keyColumns[0])).append(" ");
        int index = po.get_ColumnIndex("DocumentNo");
        if (index != -1)
            sb.append(po.get_Value(index)).append(": ");
        index = po.get_ColumnIndex("SalesRep_ID");
        Integer sr = null;
        if (index != -1)
            sr = (Integer) po.get_Value(index);
        else {
            index = po.get_ColumnIndex("AD_User_ID");
            if (index != -1)
                sr = (Integer) po.get_Value(index);
        }
        if (sr != null) {
            MUser user = new MUser(Env.getCtx(), sr.intValue(), null);
            if (user != null)
                sb.append(user.getName()).append(" ");
        }
        //
        index = po.get_ColumnIndex("C_BPartner_ID");
        if (index != -1) {
            Integer bp = (Integer) po.get_Value(index);
            if (bp != null) {
                MBPartner partner = new MBPartner(Env.getCtx(), bp.intValue(), null);
                if (partner != null)
                    sb.append(partner.getName()).append(" ");
            }
        }
        return sb.toString();
    } // getSummary

    private List<String> getDataByTableNameRecord(String ad_table_id, String record_id) {
        List<String> result = new ArrayList<>();
        MTable t = new MTable(Env.getCtx(), Integer.valueOf(ad_table_id), null);


        MColumn cDocType = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='C_DocType_ID'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cDocumentNo = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='DocumentNo'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cBPartnerId = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='C_BPartner_ID'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cDateAcct = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='DateAcct'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cDateDoc = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='DateDoc'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cGrandTotal = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='GrandTotal'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cPayAmt =
                new Query(Env.getCtx(), MColumn.Table_Name, "AD_Table_ID=? AND ColumnName='PayAmt'",
                        null).setParameters(Integer.valueOf(ad_table_id)).first();
        MColumn cCurrency = new Query(Env.getCtx(), MColumn.Table_Name,
                "AD_Table_ID=? AND ColumnName='C_Currency_ID'", null)
                        .setParameters(Integer.valueOf(ad_table_id)).first();

        String select = " select coalesce(a.description,'') description ";
        String join = " from " + t.getTableName() + " a";
        String where = " where a." + t.getTableName() + "_ID = ?";

        if (cDocType != null) {
            select += " ,coalesce(b.name,'') doctype ";
            join += " left join c_doctype b on b.c_doctype_id = a.c_doctype_id ";
        } else {
            select += " ,'' doctype ";
        }
        if (cDocumentNo != null) {
            select += " ,a.documentno ";
        } else {
            select += " ,'' document_no ";
        }
        if (cBPartnerId != null) {
            select += " ,c.name bpartner ";
            join += " left join c_bpartner c on c.c_bpartner_id = a.c_bpartner_id ";
        } else {
            select += " ,'' bpartner ";
        }
        if (cDateAcct != null) {
            select += " ,to_char(a.dateacct,'DD-MM-YYYY') docdate ";
        } else if (cDateDoc != null) {
            select += " ,to_char(a.datedoc,'DD-MM-YYYY') docdate ";
        } else {
            select += " ,'' docdate ";
        }

        String cur = "''";
        if (cCurrency != null) {
            cur = "d.cursymbol";
            join += " left join c_currency d on d.c_currency_id = a.c_currency_id ";
        }
        if (cGrandTotal != null) {
            select += " ,concat(" + cur
                    + ",' ',trim(replace(to_char(a.grandtotal,'999,999,999,999,999'),',','.')),',00') grandtotal ";
        } else if (cPayAmt != null) {
            select += " ,concat(" + cur
                    + ",' ',trim(replace(to_char(a.payamt,'999,999,999,999,999'),',','.')),',00') grandtotal ";
        } else {
            select += " ,'' grandtotal ";
        }
        

        String sql = select + join + where;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql, null);
            ps.setInt(1, Integer.valueOf(record_id));
            rs = ps.executeQuery();

            if (rs.next()) {
                result.add(rs.getString(1));
                result.add(rs.getString(2));
                result.add(rs.getString(3));
                result.add(rs.getString(4));
                result.add(rs.getString(5));
                result.add(rs.getString(6));
                result.add(getDataLine(ad_table_id,record_id));
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql, e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return result;
    }

    private String getDataLine(String ad_table_id, String record_id) {
        MTable t = new MTable(Env.getCtx(), Integer.valueOf(ad_table_id), null);
        StringBuilder result = new StringBuilder();
        result.append("");
        String whereClause = " TableName = '" + t.Table_Name + "Line' ";
        MTable tLine = new Query(Env.getCtx(), MTable.Table_Name, whereClause, null).first();

        StringBuilder select = new StringBuilder();
        StringBuilder from = new StringBuilder();
        StringBuilder whereCond = new StringBuilder();

        if (tLine != null) {
            MColumn cCurrency = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='C_Currency_ID'", null)
                            .setParameters(Integer.valueOf(ad_table_id)).first();

            MColumn product = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='M_Product_ID'", null)
                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
            MColumn uom = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='C_UOM_ID'", null)
                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
            MColumn qty_entered = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='QtyEntered'", null)
                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
            MColumn qty = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='Qty'", null)
                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
            MColumn price_entered = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='PriceEntered'", null)
                            .setParameters(Integer.valueOf(tLine.get_ID())).first();
            MColumn price_actual = new Query(Env.getCtx(), MColumn.Table_Name,
                    "AD_Table_ID=? AND ColumnName='PriceActual'", null)
                            .setParameters(Integer.valueOf(tLine.get_ID())).first();

            select.append(" select a.line ");
            from.append(" from " + tLine.Table_Name + " a");
            if (product != null) {
                select.append(" ,b.name product_name ");
                from.append(
                        " left join m_product b on b.m_product_id = a.m_product_id and b.ad_client_id = a.ad_client_id ");
            } else {
                select.append(" ,'' product_name");
            }
            if (uom != null) {
                select.append(" ,c.name uom_name ");
                from.append(
                        " left join c_uom c on c.c_uom_id = a.c_uom_id and c.ad_client_id = a.ad_client_id  ");
            } else {
                select.append(" ,'' uom_name");
            }
            if (qty_entered != null) {
                select.append(" ,a.qtyentered");
            } else if (qty != null) {
                select.append(" ,a.qty qtyentered");
            } else {
                select.append(" ,'' qtyentered");
            }

            String cur = "''";
            if (cCurrency != null) {
                cur = "d.cursymbol";
                from.append(
                        " left join " + t.Table_Name + " d on d.c_currency_id = a.c_currency_id ");
                from.append(" left join c_currency e on e.c_currency_id = d.c_currency_id ");
            }
            if (price_entered != null) {

                select.append(" ,concat(" + cur
                        + ",' ',trim(replace(to_char(a.priceentered,'999,999,999,999,999'),',','.')),',00') priceentered ");
            } else if (price_actual != null) {
                select.append(" ,concat(" + cur
                        + ",' ',trim(replace(to_char(a.priceactual,'999,999,999,999,999'),',','.')),',00') priceentered ");
            } else {
                select.append(" ,'' priceentered ");
            }

            whereCond.append(" where a." + t.getTableName() + "_ID = ?");
            String sql = select.toString() + from.toString() + whereCond.toString();
            PreparedStatement ps = null;
            ResultSet rs = null;
            try {
                ps = DB.prepareStatement(sql, null);
                ps.setInt(1,Integer.valueOf(record_id));
                rs = ps.executeQuery();
                while (rs.next()) {
                    result.append("Line No: ");
                    result.append(rs.getString(1));
                    result.append(", ");
                    result.append("Product Name: ");
                    result.append(rs.getString(2));
                    result.append(", ");
                    result.append("UOM: ");
                    result.append(rs.getString(3));
                    result.append(", ");
                    result.append("Qty: ");
                    result.append(rs.getString(4));
                    result.append(", ");
                    result.append("Price: ");
                    result.append(rs.getString(5));
                    result.append("\n");
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, sql, e);
            } finally {
                DB.close(rs);
                ps = null;
                rs = null;
            }
        }
        return result.toString();
    }

    private String getDescDetail(PO po, String tipe) {
        // DecimalFormat df = new DecimalFormat("#.##0,00");
        int index = -1;
        if (po == null)
            return null;
        StringBuilder sb = new StringBuilder();

        if (po.get_TableName().equalsIgnoreCase(MRequisition.Table_Name)) {
            index = po.get_ColumnIndex("M_Requisition_ID");
            if (index != -1) {
                try {
                    MRequisition order = new MRequisition(po.getCtx(),
                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
                    if (tipe.equals("price")) {
                        sb.append("Amount:");
                        sb.append(" ")
                                .append(new MCurrency(po.getCtx(), order.getC_Currency_ID(),
                                        po.get_TrxName()).getCurSymbol() == null
                                                ? ""
                                                : new MCurrency(po.getCtx(),
                                                        order.getC_Currency_ID(), po.get_TrxName())
                                                                .getCurSymbol())
                                .append(" ")
                                .append(toRupiahFormat(order.getTotalLines().toString()));
                    } else {
                        sb.append(order.getDescription());
                    }

                } catch (Exception e) {
                    return "";
                }
            }
        }


        if (po.get_TableName().equalsIgnoreCase(MOrder.Table_Name)) {
            index = po.get_ColumnIndex("C_Order_ID");
            if (index != -1) {
                try {
                    MOrder order = new MOrder(po.getCtx(),
                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
                    if (tipe.equals("price")) {
                        sb.append("Amount:");
                        sb.append(" ")
                                .append(order.getC_Currency().getCurSymbol() == null ? ""
                                        : order.getC_Currency().getCurSymbol())
                                .append(" ")
                                .append(toRupiahFormat(order.getGrandTotal().toString()));
                    } else {
                        sb.append(order.getDescription());
                    }
                } catch (Exception e) {
                    return "";
                }
            }
        }
        if (po.get_TableName().equalsIgnoreCase(MInvoice.Table_Name)) {
            index = po.get_ColumnIndex("C_Invoice_ID");
            if (index != -1) {
                try {
                    MInvoice order = new MInvoice(po.getCtx(),
                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
                    if (tipe.equals("price")) {
                        sb.append("Amount:");
                        sb.append(" ")
                                .append(order.getC_Currency().getCurSymbol() == null ? ""
                                        : order.getC_Currency().getCurSymbol())
                                .append(" ")
                                .append(toRupiahFormat(order.getGrandTotal().toString()));
                    } else {
                        sb.append(order.getDescription());
                    }

                } catch (Exception e) {
                    return "";
                }
            }
        }
        if (po.get_TableName().equalsIgnoreCase(MPayment.Table_Name)) {
            index = po.get_ColumnIndex("C_Payment_ID");
            if (index != -1) {
                try {
                    MPayment order = new MPayment(po.getCtx(),
                            ((Integer) po.get_Value(index)).intValue(), po.get_TrxName());
                    if (tipe.equals("price")) {
                        sb.append("Amount:");
                        sb.append(" ")
                                .append(order.getC_Currency().getCurSymbol() == null ? ""
                                        : order.getC_Currency().getCurSymbol())
                                .append(" ").append(toRupiahFormat(order.getPayAmt().toString()));
                    } else {
                        sb.append(order.getDescription());
                    }
                } catch (Exception e) {
                    return "";
                }
            }
        }
        if (po.get_TableName().equalsIgnoreCase("R_SPPD")) {
            index = po.get_ColumnIndex("R_SPPD_ID");
            if (index != -1) {
                PreparedStatement stmt = null;
                ResultSet rsl = null;
                try {
                    Integer sppd_id = ((Integer) po.get_Value(index)).intValue();
                    Integer client_id = po.get_ValueAsInt("AD_Client_ID");
                    String docno = po.get_Value("DocumentNo").toString();

                    String sqle = "SELECT * FROM R_SPPD where ad_client_id = ? and r_sppd_id=? "
                            + " and document_no=? ";


                    stmt = DB.prepareStatement(sqle, null);

                    stmt.setInt(1, client_id);
                    stmt.setInt(2, sppd_id);
                    stmt.setString(3, docno);
                    rsl = stmt.executeQuery();
                    String grndtotal = "";
                    String umtotal = "";
                    String spdesc = "";
                    String isPjk = "";
                    while (rsl.next()) {
                        grndtotal = rsl.getString("grandtotal");
                        spdesc = rsl.getString("description");
                        umtotal = rsl.getString("payamt");
                        isPjk = rsl.getString("ispjk");
                    }

                    if (tipe.equals("price")) {
                        if (isPjk.equals("Y")) {
                            sb.append("PJK:");
                            sb.append(" ").append(toRupiahFormat(grndtotal.toString()));
                        } else {
                            sb.append("UM:");
                            sb.append(" ").append(toRupiahFormat(umtotal.toString()));
                        }

                    } else {
                        sb.append(spdesc);
                    }
                } catch (Exception e) {
                    return "";
                } finally {
                    DB.close(rsl, stmt);
                    rsl = null;
                    stmt = null;
                }
            }
        }
        return sb.toString();
    }// getDescDetail


}


