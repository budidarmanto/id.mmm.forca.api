package id.mmm.forca.api.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.adempiere.exceptions.ProductNotOnPriceListException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCampaign;
import org.compiere.model.MLocation;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MTaxCategory;
import org.compiere.model.MUOM;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.model.X_C_Channel;
import org.compiere.util.Env;
import org.compiere.util.Util;
import id.mmm.forca.api.request.ParamRequest2;
import id.mmm.forca.api.util.WsUtil;
import id.sisi.forca.authenticationapi.response.ResponseData;

public class ForcaCrmCormaImpl {
    public List<Object> listdata = new ArrayList<Object>();
    ResponseData resp = new ResponseData();
    Map<String, Object> map;

    public ResponseData actSetPartner(ParamRequest2 param) {
        List<Object> listdata = new ArrayList<Object>();
        try {
            if (param.getC_bpartner_id().intValue() > 0) {
                MBPartner bp =
                        new MBPartner(Env.getCtx(), param.getC_bpartner_id().intValue(), null);

                bp.setName(param.getName_partner());
                bp.setC_BP_Group_ID(WsUtil.defaultBPGroup(Env.getCtx()));
                bp.setSalesVolume((int) param.getSalesvolume());
                bp.setReferenceNo(param.getReferenceno());
                bp.set_ValueOfColumn("crm_id", param.getCrm_id());
                if (bp.save()) {
                    String prmtr = "AD_Client_ID = " + bp.getAD_Client_ID() + " AND C_BPartner_ID="
                            + bp.getC_BPartner_ID();
                    Query q = new Query(Env.getCtx(), "C_BPartner_Location", prmtr, null);
                    q.setOnlyActiveRecords(true);
                    MBPartnerLocation bploc = (MBPartnerLocation) q.first();
                    MLocation loc;
                    if (bploc == null) {
                        loc = new MLocation(Env.getCtx(), 0, null);
                    } else {
                        loc = new MLocation(Env.getCtx(), bploc.getC_Location_ID(), null);
                    }
                    loc.setAddress1(param.getAddress1());
                    loc.setAddress2(param.getAddress2());
                    loc.setAddress3(param.getAddress3());
                    loc.setAddress4(param.getAddress4());
                    loc.setC_Country_ID(param.getC_country_id().intValue());
                    loc.setC_Region_ID(param.getC_region_id().intValue());
                    loc.setC_City_ID(param.getC_city_id().intValue());
                    if (loc.save()) {
                        if (bploc == null) {
                            MBPartnerLocation bplocation =
                                    new MBPartnerLocation(Env.getCtx(), 0, null);
                            bplocation.setName(param.getName_partner());
                            bplocation.setC_BPartner_ID(bp.getC_BPartner_ID());
                            bplocation.setC_Location_ID(loc.getC_Location_ID());
                            bplocation.saveEx();
                        }
                        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                        xmap.put("c_bpartner_id", Integer.valueOf(bp.getC_BPartner_ID()));
                        xmap.put("partner_value", bp.getValue());
                        xmap.put("c_bpartner_location_id",
                                Integer.valueOf(bploc.getC_BPartner_Location_ID()));
                        xmap.put("c_location_id", Integer.valueOf(loc.getC_Location_ID()));
                        xmap.put("crm_id", bp.get_Value("crm_id"));
                        listdata.add(xmap);
                        this.resp.setCodestatus("S");
                        this.resp.setMessage("Success");
                        this.resp.setResultdata(listdata);
                    } else {
                        this.resp.setMessage("Cannot save Location");
                    }
                }
            } else {
                MBPartner partner = new MBPartner(Env.getCtx(), 0, null);
                partner.setName(param.getName_partner());
                if (!Util.isEmpty(param.getValue_partner())) {
                    partner.setValue(param.getValue_partner());
                }
                if (param.getC_bp_status_id() != null) {
                    partner.set_ValueOfColumn("c_bp_status_id", param.getC_bp_status_id());
                }
                partner.setSalesVolume((int) param.getSalesvolume());

                partner.setIsCustomer(true);
                partner.set_ValueOfColumn("crm_id", param.getCrm_id());
                if (partner.save()) {
                    MLocation location = new MLocation(Env.getCtx(), 0, null);
                    location.setAddress1(param.getAddress1());
                    location.setAddress2(param.getAddress2());
                    location.setAddress3(param.getAddress3());
                    location.setAddress4(param.getAddress4());
                    location.setC_Country_ID(param.getC_country_id().intValue());
                    location.setC_Region_ID(param.getC_region_id().intValue());
                    location.setC_City_ID(param.getC_city_id().intValue());
                    if (location.save()) {
                        MBPartnerLocation bpLoc = new MBPartnerLocation(Env.getCtx(), 0, null);
                        bpLoc.setName(param.getName_partner());
                        bpLoc.setC_BPartner_ID(partner.getC_BPartner_ID());
                        bpLoc.setC_Location_ID(location.getC_Location_ID());
                        bpLoc.saveEx();
                        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                        xmap.put("c_bpartner_id", Integer.valueOf(partner.getC_BPartner_ID()));
                        xmap.put("partner_value", partner.getValue());
                        xmap.put("c_bpartner_location_id",
                                Integer.valueOf(bpLoc.getC_BPartner_Location_ID()));
                        xmap.put("c_location_id", Integer.valueOf(location.getC_Location_ID()));
                        xmap.put("crm_id", partner.get_Value("crm_id"));

                        listdata.add(xmap);
                        this.resp.setCodestatus("S");
                        this.resp.setMessage("Success");
                        this.resp.setResultdata(listdata);
                    } else {
                        this.resp.setMessage("Cannot save Location Partner");
                    }
                } else {
                    this.resp.setMessage("Cannot save Customer");
                }
            }
        } catch (Exception e) {
            this.resp.setCodestatus("E");
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actSetUserContact(ParamRequest2 param) {
        List<Object> listdata = new ArrayList<Object>();
        try {
            if (param.getC_bpartner_id().intValue() > 0) {
                MUser user;
                if (param.getAd_user_id().intValue() > 0) {
                    user = new MUser(Env.getCtx(), param.getAd_user_id().intValue(), null);
                } else {
                    user = new MUser(Env.getCtx(), 0, null);
                }
                user.setC_BPartner_ID(param.getC_bpartner_id().intValue());
                user.setName(param.getName());
                user.setEMail(param.getEmail_contact());
                user.setPhone(param.getPhone_contact());
                user.setPhone2(param.getPhone2_contact());
                user.setFax(param.getFax());
                user.setTitle(param.getTitle());
                user.set_ValueOfColumn("crm_id", param.getCrm_id());
                if (user.save()) {
                    Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                    xmap.put("c_bpartner_id", Integer.valueOf(user.getC_BPartner_ID()));
                    xmap.put("ad_user_id", Integer.valueOf(user.getAD_User_ID()));
                    xmap.put("name", user.getName());
                    xmap.put("crm_id", user.get_Value("crm_id"));
                    listdata.add(xmap);
                    this.resp.setCodestatus("S");
                    this.resp.setMessage("Success");
                    this.resp.setResultdata(listdata);
                } else {
                    this.resp.setMessage("Cannot Save Contact Customer");
                }
            } else {
                this.resp.setMessage("Failed, Parameter Required");
            }
        } catch (Exception e) {
            this.resp.setCodestatus("E");
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actSetChannel(ParamRequest2 param) {
        try {
            X_C_Channel canel = null;
            if (param.getC_channel_id().intValue() > 0) {
                canel = new X_C_Channel(Env.getCtx(), param.getC_channel_id().intValue(), null);
            } else {
                canel = new X_C_Channel(Env.getCtx(), 0, null);
            }
            canel.setAD_Org_ID(Env.getAD_Org_ID(Env.getCtx()));
            canel.setName(param.getName());
            canel.setDescription(param.getDescription());
            canel.setIsActive(true);
            canel.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (canel.save()) {
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success Transaction");
                this.map = new LinkedHashMap<String, Object>();
                this.map.put("c_channel_id", Integer.valueOf(canel.getC_Channel_ID()));
                this.map.put("crm_id", canel.get_Value("crm_id"));
                this.listdata.add(this.map);
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Failed Transaction");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetCampaign(ParamRequest2 param) {
        try {
            MCampaign campaign = null;
            if (param.getC_campaign_id().intValue() > 0) {
                campaign = new MCampaign(Env.getCtx(), param.getC_campaign_id().intValue(), null);
            } else {
                campaign = new MCampaign(Env.getCtx(), 0, null);
            }
            campaign.setAD_Org_ID(0);
            campaign.setName(param.getName());
            campaign.setDescription(param.getDescription());
            campaign.setC_Channel_ID(param.getC_channel_id().intValue());
            if (!Util.isEmpty(param.getDatefrom())) {
                campaign.setStartDate(WsUtil.convertFormatTgl(param.getDatefrom()));
            }
            if (!Util.isEmpty(param.getDateto())) {
                campaign.setEndDate(WsUtil.convertFormatTgl(param.getDateto()));
            }
            campaign.setCosts(new BigDecimal(param.getCost()));
            campaign.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (campaign.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("c_campaign_id", Integer.valueOf(campaign.getC_Campaign_ID()));
                xmap.put("name", campaign.getName());
                xmap.put("crm_id", campaign.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil update");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Gagal Save Data");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetSalesOrder(ParamRequest2 param) {
        if (param.getAd_orgtrx_id().intValue() == 0) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        MOrder order = null;
        try {
            if (param.getC_order_id().intValue() > 0) {
                order = new MOrder(Env.getCtx(), param.getC_order_id().intValue(), null);
            } else {
                order = new MOrder(Env.getCtx(), 0, null);
            }
            order.setAD_Org_ID(param.getAd_org_id().intValue());
            order.setC_BPartner_ID(param.getC_bpartner_id().intValue());
            order.setC_BPartner_Location_ID(param.getC_bpartner_location_id().intValue());
            if (param.getM_warehouse_id().intValue() > 0) {
                order.setM_Warehouse_ID(param.getM_warehouse_id().intValue());
            } else {
                order.setM_Warehouse_ID(WsUtil.defaultWarehouseSISI(Env.getCtx()));
            }
            if (param.getC_opportunity_id().intValue() > 0) {
                order.setC_Opportunity_ID(param.getC_opportunity_id().intValue());
            }
            if (param.getC_campaign_id().intValue() > 0) {
                order.setC_Campaign_ID(param.getC_campaign_id().intValue());
            }
            if (param.getDocumentno() != null) {
                order.setDocumentNo(param.getDocumentno());
            }
            if (param.getAd_user_id().intValue() > 0) {
                order.setAD_User_ID(param.getAd_user_id().intValue());
            }
            order.setDateOrdered(WsUtil.convertFormatTgl(param.getDatefrom()));
            order.setC_DocTypeTarget_ID("SO");
            order.setM_PriceList_ID(WsUtil.defaultPriceJual(Env.getCtx()));
            order.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
            order.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (order.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("crm_id", order.get_Value("crm_id"));
                xmap.put("c_order_id", Integer.valueOf(order.getC_Order_ID()));
                xmap.put("documentno", order.getDocumentNo());

                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success insert or update data order");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Gagal save Sales Order");
            }
        } catch (ProductNotOnPriceListException e) {
            order.deleteEx(true);
            this.resp.setMessage(e.getStackTrace().toString());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetSalesOrderLine(ParamRequest2 param) {
        if ((param.getC_order_id().intValue() <= 0) || (param.getAd_orgtrx_id().intValue() == 0)) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        MOrder order = null;
        MOrderLine orderLine = null;
        try {
            order = new MOrder(Env.getCtx(), param.getC_order_id().intValue(), null);
            if (param.getC_orderline_id().intValue() > 0) {
                orderLine =
                        new MOrderLine(Env.getCtx(), param.getC_orderline_id().intValue(), null);
                orderLine.setC_Order_ID(order.getC_Order_ID());
            } else {
                orderLine = new MOrderLine(order);
            }
            orderLine.setOrder(order);
            orderLine.setAD_Org_ID(order.getAD_Org_ID());

            orderLine.setM_Product_ID(param.getM_product_id().intValue());

            orderLine.setQty(new BigDecimal(param.getQty()));
            orderLine.setPriceActual(new BigDecimal(param.getPriceactual()));
            orderLine.setC_UOM_ID(param.getC_uom_id().intValue());
            orderLine.set_ValueOfColumn("crm_id", param.getCrm_id());
            orderLine.set_ValueOfColumn("ad_orgtrx_id", param.getAd_orgtrx_id());
            if (orderLine.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("crm_id", orderLine.get_Value("crm_id"));
                xmap.put("c_orderline_id", Integer.valueOf(orderLine.getC_OrderLine_ID()));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success insert or update data order lines");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Gagal save data");
            }
        } catch (ProductNotOnPriceListException e) {
            order.deleteEx(true);
            this.resp.setMessage(e.getStackTrace().toString());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetProduct(ParamRequest2 param) {
        if (param.getName() == null) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MProduct produk = null;
            if (param.getM_product_id().intValue() > 0) {
                produk = new MProduct(Env.getCtx(), param.getM_product_id().intValue(), null);
            } else {
                produk = new MProduct(Env.getCtx(), 0, null);
            }
            produk.setAD_Org_ID(0);
            produk.setName(param.getName());
            produk.setDescription(param.getDescription());
            produk.setProductType(param.getProducttype());
            produk.setIsSold(WsUtil.convertYesNo(param.getIssold()).booleanValue());
            produk.setIsPurchased(WsUtil.convertYesNo(param.getIspurchased()).booleanValue());
            produk.setC_UOM_ID(param.getC_uom_id().intValue());
            produk.setIsActive(WsUtil.convertYesNo(param.getIsactive()).booleanValue());
            produk.setM_Product_Category_ID(param.getM_product_category_id().intValue());
            produk.setC_TaxCategory_ID(param.getC_taxcategory_id().intValue());
            produk.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (produk.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("m_product_id", Integer.valueOf(produk.getM_Product_ID()));
                xmap.put("value", produk.getValue());
                xmap.put("name", produk.getName());
                xmap.put("crm_id", produk.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success update data product");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Cannot save product");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetCrmProduct(ParamRequest2 param) {
        if ((param.getCrm_id().intValue() == 0) || (param.getM_product_id().intValue() == 0)) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MProduct produk = new MProduct(Env.getCtx(), param.getM_product_id().intValue(), null);
            produk.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (produk.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("m_product_id", Integer.valueOf(produk.getM_Product_ID()));
                xmap.put("crm_id", produk.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success update data product");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Cannot save crm_id to product");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetCrmOrg(ParamRequest2 param) {
        if ((param.getCrm_id().intValue() == 0) || (param.getAd_org_id().intValue() == 0)) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MOrg org = new MOrg(Env.getCtx(), param.getAd_org_id().intValue(), null);
            org.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (org.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("ad_org_id", Integer.valueOf(org.getAD_Org_ID()));
                xmap.put("crm_id", org.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success update data organization");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Cannot save crm_id to organization");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetCrmProductCategory(ParamRequest2 param) {
        if ((param.getCrm_id().intValue() == 0)
                || (param.getM_product_category_id().intValue() == 0)) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MProductCategory produk = new MProductCategory(Env.getCtx(),
                    param.getM_product_category_id().intValue(), null);
            produk.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (produk.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("ad_org_id", Integer.valueOf(produk.getAD_Org_ID()));
                xmap.put("crm_id", produk.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success update data product category");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Cannot save crm_id to product category");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetCrmUom(ParamRequest2 param) {
        if ((param.getCrm_id().intValue() == 0) || (param.getC_uom_id().intValue() == 0)) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MUOM produk = new MUOM(Env.getCtx(), param.getC_uom_id().intValue(), null);
            produk.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (produk.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("c_uom_id", Integer.valueOf(produk.getC_UOM_ID()));
                xmap.put("crm_id", produk.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success update data uom");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Cannot save crm_id to uom");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetCrmTaxCategory(ParamRequest2 param) {
        if ((param.getCrm_id().intValue() == 0) || (param.getC_taxcategory_id().intValue() == 0)) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MTaxCategory produk =
                    new MTaxCategory(Env.getCtx(), param.getC_taxcategory_id().intValue(), null);
            produk.set_ValueOfColumn("crm_id", param.getCrm_id());
            if (produk.save()) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("c_taxcategory_id", Integer.valueOf(produk.getC_TaxCategory_ID()));
                xmap.put("crm_id", produk.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success update data Tax Category");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Cannot save crm_id to Tax Category");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
            return this.resp;
        }
        return this.resp;
    }

    public ResponseData actSetStatusOrder(ParamRequest2 param, String action) {
        if (param.getC_order_id().intValue() == 0) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        MOrder order = null;
        try {
            order = new MOrder(Env.getCtx(), param.getC_order_id().intValue(), null);
            if (action.equals("VIEW")) {
                Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                xmap.put("docstatus", order.getDocStatus());
                xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), order.getDocStatus()));
                xmap.put("docaction", order.getDocAction());
                xmap.put("crm_id", order.get_Value("crm_id"));
                this.listdata.add(xmap);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Success view Doc Status Order");
                this.resp.setResultdata(this.listdata);
            } else if (action.equals("CO")) {
                order.setDocStatus("CO");
                order.setDocAction("CO");
                order.saveEx();
                order.completeIt();
                if (order.isComplete()) {
                    Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                    xmap.put("docstatus", order.getDocStatus());
                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), order.getDocStatus()));
                    xmap.put("docaction", order.getDocAction());
                    xmap.put("crm_id", order.get_Value("crm_id"));
                    this.listdata.add(xmap);
                    this.resp.setCodestatus("S");
                    this.resp.setMessage("Success Complete Order");
                    this.resp.setResultdata(this.listdata);
                } else {
                    this.resp.setMessage("Failed Complete Order");
                }
            } else if (action.equals("VO")) {
                order.setDocStatus("VO");
                order.setDocAction("VO");
                order.saveEx();
                if (order.voidIt()) {
                    Map<String, Object> xmap = new LinkedHashMap<String, Object>();
                    xmap.put("docstatus", order.getDocStatus());
                    xmap.put("status", WsUtil.descDocStatus(Env.getCtx(), order.getDocStatus()));
                    xmap.put("docaction", order.getDocAction());
                    xmap.put("crm_id", order.get_Value("crm_id"));
                    this.listdata.add(xmap);
                    this.resp.setCodestatus("S");
                    this.resp.setMessage("Success Void Order");
                    this.resp.setResultdata(this.listdata);
                } else {
                    this.resp.setMessage("Failed Void Order");
                }
            }
        } catch (Throwable e) {
            if (e.getMessage() == null) {
                this.resp.setMessage(order.get_Logger().toString());
            } else {
                this.resp.setMessage(e.getMessage());
            }
        }
        return this.resp;
    }
}
