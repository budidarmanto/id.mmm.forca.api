package id.mmm.forca.api.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCurrency;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MLocation;
import org.compiere.model.MOrder;
import org.compiere.model.MOrderLine;
import org.compiere.model.MOrg;
import org.compiere.model.MPriceList;
import org.compiere.model.MTax;
import org.compiere.model.MUOM;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.wf.MWorkflow;
import id.mmm.forca.api.request.ParamInvoice;
import id.mmm.forca.api.request.ParamInvoiceLine;
import id.mmm.forca.api.request.ParamOrder;
import id.mmm.forca.api.request.ParamOrderLine;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.response.ResponseData;

public class ForcaOrderImpl {
    ResponseData resp = new ResponseData();

    WebserviceUtil wu = new WebserviceUtil();
    AuthLoginImpl authLoginImpl = new AuthLoginImpl();
    private CLogger log = CLogger.getCLogger(getClass());
    public Properties wsenv = new Properties();

    public Properties getDefaultCtx() {
        wsenv.setProperty("#AD_Language", "en_US");
        wsenv.setProperty("context", "deverp.semenindonesia.com/ws");
        return wsenv;
    }

    public ResponseData insertSalesOrder(ParamOrder param, String token) {
        
//        {
//            "document_no": "0002",
//            "m_pricelist_id": "1000052",
//            "salerep_id": "1000008",
//            "payment_rule": "P",
//            "list_line": [
//              {
//                "qty_entered": "100",
//                "price_entered": "100000",
//                "c_tax_id": "1000022",
//                "m_product_id": "1000333"
//              }
//            ],
//            "m_warehouse_id": "1000028",
//            "c_bpartner_id": "1000100",
//            "ad_org_id": "1000172"
//          }
        
        MOrder o = null;
        String trxName = Trx.createTrxName("IV");
        Trx trx = Trx.get(trxName, true);

        ParamRequest param2 = new ParamRequest();
        param2.setToken(token);
        Map<String, Object> mToken =
                (Map<String, Object>) authLoginImpl.infoToken(param2).getResultdata();
        Integer ad_client_id = (Integer) mToken.get("ad_client_id");
        Integer ad_user_id = (Integer) mToken.get("ad_user_id");

        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        Env.setCtx(wsenv);

        try {
            // Integer C_DocTypeTarget_ID = Integer.valueOf(param.getC_DocTypeTarget_ID());
            String DocumentNo = param.getDocument_no();
            Integer C_BPartner_ID = Integer.valueOf(param.getC_bpartner_id());
            Integer AD_Org_ID = Integer.valueOf((param.getAd_org_id()));
            Integer M_Warehouse_ID = Integer.valueOf(param.getM_warehouse_id());
            Integer SalesRep_ID = Integer.valueOf(param.getSalerep_id());
            String PaymentRule = param.getPayment_rule();
            Integer M_PriceList_ID = Integer.valueOf(param.getM_pricelist_id());
            List<ParamOrderLine> listLine = param.getList_line();

            Integer line = 10;

            String whereClause;
            whereClause = " ISO_Code = 'IDR' ";
            MCurrency cur =
                    new Query(Env.getCtx(), MCurrency.Table_Name, whereClause, trxName).first();

            whereClause = " Name = 'Standard Order' AND AD_Client_ID = ? ";
            MDocType docType = new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();

            whereClause = " Name = 'Harga Beli Include' AND AD_Client_ID = ? ";
            MPriceList priceList =
                    new Query(Env.getCtx(), MPriceList.Table_Name, whereClause, trxName)
                            .setParameters(ad_client_id).first();

            whereClause = " Name = 'ssm26' AND AD_Client_ID = ? ";
            MUser uRep = new Query(Env.getCtx(), MUser.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();

            whereClause = " Name = 'VZ' AND AD_Client_ID = ? ";
            MTax tax = new Query(Env.getCtx(), MTax.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();

            whereClause = " C_BPartner_ID = ?";
            MBPartnerLocation bpLoc =
                    new Query(Env.getCtx(), MBPartnerLocation.Table_Name, whereClause, trxName)
                            .setParameters(Integer.valueOf(C_BPartner_ID)).first();

            whereClause = " Name = 'Each' AND AD_Client_ID = ? ";
            MUOM uom = new Query(Env.getCtx(), MUOM.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();


            // create header
            o = new MOrder(Env.getCtx(), 0, trxName);
            o.setAD_Org_ID(AD_Org_ID);
            o.setDocumentNo(DocumentNo);
            o.setC_DocTypeTarget_ID(docType.get_ID());
            o.setDateOrdered(new Timestamp(new Date().getTime()));
            o.setC_BPartner_ID(C_BPartner_ID);
            o.setC_BPartner_Location_ID(bpLoc.get_ID());
            o.setM_Warehouse_ID(M_Warehouse_ID);
            o.setM_PriceList_ID(M_PriceList_ID);
            o.setSalesRep_ID(SalesRep_ID);
            o.setPaymentRule(PaymentRule);
            o.setC_Currency_ID(cur.get_ID());
            o.setIsSOTrx(true);
            o.saveEx();

            for (ParamOrderLine pol : listLine) {

                // create detail
                MOrderLine ol = new MOrderLine(Env.getCtx(), 0, trxName);
                ol.setAD_Org_ID(AD_Org_ID);
                ol.setC_Order_ID(o.get_ID());
                ol.setLine(line);
                ol.setM_Product_ID(Integer.valueOf(pol.getM_product_id()));
//                ol.setC_UOM_ID(uom.get_ID());
                ol.setQtyEntered(new BigDecimal(pol.getQty_entered()));
                ol.setQtyOrdered(new BigDecimal(pol.getQty_entered()));
                ol.setPriceEntered(new BigDecimal(pol.getPrice_entered()));
                ol.setPriceActual(new BigDecimal(pol.getPrice_entered()));
                ol.setC_Tax_ID(Integer.valueOf(pol.getC_tax_id()));
                ol.saveEx();
                
                line += 10;

            }

//            MWorkflow.runDocumentActionWorkflow(o, DocAction.ACTION_Complete);
            resp.setCodestatus("S");
            resp.setMessage("Insert Successed");
            Map<String,Object> xmap = new LinkedHashMap<>();
            xmap.put("c_order_id", o.get_ID());
            xmap.put("documentno", o.getDocumentNo());
            resp.setResultdata(xmap);
        } catch (AdempiereException e) {
            resp.setCodestatus("E");
            resp.setMessage("Failed caused by : " + e.getMessage());
            trx.rollback();
        } catch (Exception e) {
            resp.setCodestatus("E");
            resp.setMessage("Failed caused by : " + e.getMessage());
            trx.rollback();
        } finally {
            if (trx.isActive()) {
                trx.commit();
            }
            trx.close();
        }

        return resp;
    }

    // SQL
    private String getPaymentRule(String trxName) {
        String result = "";

        // @formatter:off
        String sql = 
                "select " + 
                "    b.ad_ref_list_id " + 
                "from ad_reference a " + 
                "inner join ad_ref_list b on b.ad_reference_id = a.ad_reference_id " + 
                "where lower(a.name) like lower('%_Payment Rule%') " + 
                "and lower(b.name) like lower('%Check%')"
                ;
        // @formatter:on

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql, trxName);
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("ad_ref_list_id");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql, e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return result;
    }

}


