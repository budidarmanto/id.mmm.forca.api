package id.mmm.forca.api.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.compiere.model.MBPGroup;
import org.compiere.model.MBPartner;
import org.compiere.model.MOrg;
import org.compiere.model.MProduct;
import org.compiere.model.MProductCategory;
import org.compiere.model.MProject;
import org.compiere.model.MTaxCategory;
import org.compiere.model.MUOM;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.Env;
import id.mmm.forca.api.request.ParamRequest2;
import id.sisi.forca.authenticationapi.response.ResponseData;

public class ForcaMasterImpl {
    public List<Object> listdata = new ArrayList<Object>();
    ResponseData resp = new ResponseData();
    Map<String, Object> map;

    public ResponseData actGetBPartnerByID(ParamRequest2 param) {
        Integer bpartner = param.getC_bpartner_id();
        if (bpartner == null) {
            this.resp.setMessage("Parameter Required");
            return this.resp;
        }
        try {
            MBPartner bp = new MBPartner(Env.getCtx(), bpartner.intValue(), null);
            if (bp.getC_BPartner_ID() > 0) {
                this.map = new LinkedHashMap<String, Object>();
                this.map.put("bpname", bp.getName());
                this.map.put("bpname2", bp.getName2());
                this.map.put("value", bp.getValue());
                this.map.put("description", bp.getDescription());
                this.map.put("ad_org_id", Integer.valueOf(bp.getAD_Org_ID()));
                this.listdata.add(this.map);
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setMessage("Data Tidak Ditemukan");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetBPGroup(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ? ";
        try {
            if (param.getC_bp_group_id().intValue() > 0) {
                prmtr = prmtr + "and c_bp_group_id = " + param.getC_bp_group_id();
            }
            Query q = new Query(Env.getCtx(), "C_BP_Group", prmtr, null);
            q.setOnlyActiveRecords(true);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            List<MBPGroup> bplist = q.list();
            if (bplist.size() > 0) {
                for (int i = 0; i < bplist.size(); i++) {
                    MBPGroup bpg = (MBPGroup) bplist.get(i);
                    this.map = new LinkedHashMap<String, Object>();

                    this.map.put("c_bp_group_id", Integer.valueOf(bpg.getC_BP_Group_ID()));
                    this.map.put("value", bpg.getValue());
                    this.map.put("name", bpg.getName());
                    this.map.put("description", bpg.getDescription());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetWarehouse(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ? ";
        try {
            if (param.getM_warehouse_id().intValue() > 0) {
                prmtr = prmtr + "and m_warehouse_id = " + param.getM_warehouse_id();
            }
            if (param.getName_search() != null) {
                prmtr = prmtr + "and name ilike = '%" + param.getName_search() + "%'";
            }
            Query q = new Query(Env.getCtx(), "M_Warehouse", prmtr, null);
            q.setOnlyActiveRecords(true);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            List<MWarehouse> whlist = q.list();
            if (whlist.size() > 0) {
                for (int i = 0; i < whlist.size(); i++) {
                    MWarehouse whse = (MWarehouse) whlist.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", whse.get_Value("crm_id"));
                    this.map.put("m_warehouse_id", Integer.valueOf(whse.getM_Warehouse_ID()));
                    this.map.put("value", whse.getValue());
                    this.map.put("name", whse.getName());
                    this.map.put("description", whse.getDescription());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetProduct(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ? ";
        try {
            if (param.getM_product_id().intValue() > 0) {
                prmtr = prmtr + " and  m_product_id = " + param.getM_product_id();
            }
            if (param.getName_search() != null) {
                prmtr = prmtr + " and name ilike = '%" + param.getName_search() + "%'";
            }
            if (param.getIsproductcrm() != null) {
                prmtr = prmtr + " and isproductcrm = '" + param.getIsproductcrm() + "'";
            }
            if (param.getIssold() != null) {
                prmtr = prmtr + " and issold = '" + param.getIssold() + "'";
            }
            Query q = new Query(Env.getCtx(), "M_Product", prmtr, null);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            q.setOnlyActiveRecords(true);
            List<MProduct> prdlist = q.list();
            if (prdlist.size() > 0) {
                for (int i = 0; i < prdlist.size(); i++) {
                    MProduct prd = (MProduct) prdlist.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", prd.get_Value("crm_id"));
                    this.map.put("m_product_id", Integer.valueOf(prd.getM_Product_ID()));
                    this.map.put("m_product_category_id",
                            Integer.valueOf(prd.getM_Product_Category_ID()));
                    this.map.put("name", prd.getName());
                    this.map.put("description", prd.getDescription());
                    this.map.put("value", prd.getValue());
                    this.map.put("c_uom_id", Integer.valueOf(prd.getC_UOM_ID()));
                    this.map.put("c_taxcategory_id", Integer.valueOf(prd.getC_TaxCategory_ID()));
                    this.map.put("m_locator_id", Integer.valueOf(prd.getM_Locator_ID()));
                    this.map.put("uomsymbol", prd.getUOMSymbol());
                    this.map.put("volume", prd.getVolume());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetUOM(ParamRequest2 param) {
        String prmtr = "isactive = 'Y' ";
        try {
            if (param.getC_uom_id().intValue() > 0) {
                prmtr = prmtr + "and c_uom_id = " + param.getC_uom_id();
            }
            if (param.getName_search() != null) {
                prmtr = prmtr + "and name ilike = '%" + param.getName_search() + "%'";
            }
            Query q = new Query(Env.getCtx(), "C_UOM", prmtr, null);

            List<MUOM> uomlist = q.list();
            if (uomlist.size() > 0) {
                for (int i = 0; i < uomlist.size(); i++) {
                    MUOM uom = (MUOM) uomlist.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", uom.get_Value("crm_id"));
                    this.map.put("c_uom_id", Integer.valueOf(uom.getC_UOM_ID()));
                    this.map.put("uomsymbol", uom.getUOMSymbol());
                    this.map.put("name", uom.getName());
                    this.map.put("description", uom.getDescription());
                    this.map.put("stdprecision", Integer.valueOf(uom.getStdPrecision()));
                    this.map.put("costingprecision", Integer.valueOf(uom.getCostingPrecision()));

                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetProductCategory(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ? ";
        try {
            if (param.getM_product_category_id().intValue() > 0) {
                prmtr = prmtr + "and M_product_category_id = " + param.getM_product_category_id();
            }
            Query q = new Query(Env.getCtx(), "M_Product_Category", prmtr, null);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            q.setOnlyActiveRecords(true);
            List<MProductCategory> prdcat = q.list();
            if (prdcat.size() > 0) {
                for (int i = 0; i < prdcat.size(); i++) {
                    MProductCategory prd = (MProductCategory) prdcat.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", prd.get_Value("crm_id"));
                    this.map.put("m_product_category_id",
                            Integer.valueOf(prd.getM_Product_Category_ID()));
                    this.map.put("name", prd.getName());
                    this.map.put("description", prd.getDescription());
                    this.map.put("value", prd.getValue());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetTaxCategory(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ? ";
        try {
            if (param.getC_taxcategory_id().intValue() > 0) {
                prmtr = prmtr + "and c_taxcategory_id = " + param.getC_taxcategory_id();
            }
            Query q = new Query(Env.getCtx(), "C_TaxCategory", prmtr, null);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            q.setOnlyActiveRecords(true);
            List<MTaxCategory> taxcat = q.list();
            if (taxcat.size() > 0) {
                for (int i = 0; i < taxcat.size(); i++) {
                    MTaxCategory prd = (MTaxCategory) taxcat.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", prd.get_Value("crm_id"));
                    this.map.put("c_taxcategory_id", Integer.valueOf(prd.getC_TaxCategory_ID()));
                    this.map.put("name", prd.getName());
                    this.map.put("description", prd.getDescription());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetOrg(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ? ";
        try {
            if (param.getAd_org_id().intValue() > 0) {
                prmtr = prmtr + "and ad_org_id = " + param.getAd_org_id();
            }
            Query q = new Query(Env.getCtx(), "AD_Org", prmtr, null);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            q.setOnlyActiveRecords(true);
            List<MOrg> orglist = q.list();
            if (orglist.size() > 0) {
                for (int i = 0; i < orglist.size(); i++) {
                    MOrg org = (MOrg) orglist.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", org.get_Value("crm_id"));
                    this.map.put("ad_org_id", Integer.valueOf(org.getAD_Org_ID()));
                    this.map.put("name", org.getName());
                    this.map.put("description", org.getDescription());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetOrgTrx(ParamRequest2 param) {
        String prmtr =
                "AD_Client_ID = ? and AD_Org_ID <> 0  AND IsSummary='N' AND IsOrgTrxDim='Y' ";
        try {
            if (param.getAd_org_id().intValue() > 0) {
                prmtr = prmtr + "and ad_org_id = " + param.getAd_org_id();
            }
            Query q = new Query(Env.getCtx(), "AD_Org", prmtr, null);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            q.setOnlyActiveRecords(true);
            List<MOrg> orglist = q.list();
            if (orglist.size() > 0) {
                for (int i = 0; i < orglist.size(); i++) {
                    MOrg org = (MOrg) orglist.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("crm_id", org.get_Value("crm_id"));
                    this.map.put("ad_orgtrx_id", Integer.valueOf(org.getAD_Org_ID()));
                    this.map.put("name", org.getName());
                    this.map.put("description", org.getDescription());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }

    public ResponseData actGetProject(ParamRequest2 param) {
        String prmtr = "AD_Client_ID = ?";
        try {
            if (param.getC_project_id() > 0) {
                prmtr = prmtr + " and c_project_id = " + param.getC_project_id();
            }
            Query q = new Query(Env.getCtx(), MProject.Table_Name, prmtr, null);
            q.setParameters(new Object[] {Integer.valueOf(Env.getAD_Client_ID(Env.getCtx()))});
            q.setOnlyActiveRecords(true);
            List<MProject> prjlist = q.list();
            if (prjlist.size() > 0) {
                for (int i = 0; i < prjlist.size(); i++) {
                    MProject prj = (MProject) prjlist.get(i);
                    this.map = new LinkedHashMap<String, Object>();
                    this.map.put("c_project_id", Integer.valueOf(prj.getC_Project_ID()));
                    this.map.put("name", prj.getName());
                    this.map.put("description", prj.getDescription());
                    this.map.put("plannedamt", prj.getPlannedAmt());
                    this.map.put("plannedmarginamt", prj.getPlannedMarginAmt());
                    this.map.put("plannedqty", prj.getPlannedQty());
                    this.map.put("committedamt", prj.getCommittedAmt());
                    this.map.put("committedqty", prj.getCommittedQty());
                    this.listdata.add(this.map);
                }
                this.resp.setCodestatus("S");
                this.resp.setMessage("Berhasil");
                this.resp.setResultdata(this.listdata);
            } else {
                this.resp.setCodestatus("E");
                this.resp.setMessage("Data Not Found");
            }
        } catch (Exception e) {
            this.resp.setMessage(e.getMessage());
        }
        return this.resp;
    }
}
