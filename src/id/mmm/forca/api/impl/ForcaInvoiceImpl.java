package id.mmm.forca.api.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MBPartner;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCurrency;
import org.compiere.model.MDocType;
import org.compiere.model.MInvoice;
import org.compiere.model.MInvoiceLine;
import org.compiere.model.MLocation;
import org.compiere.model.MOrg;
import org.compiere.model.MPriceList;
import org.compiere.model.MTax;
import org.compiere.model.MUOM;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.process.DocAction;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.Trx;
import org.compiere.util.Util;
import org.compiere.wf.MWorkflow;
import id.mmm.forca.api.request.ParamInvoice;
import id.mmm.forca.api.request.ParamInvoiceLine;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.response.ResponseData;

public class ForcaInvoiceImpl {
    ResponseData resp = new ResponseData();
    
    WebserviceUtil wu = new WebserviceUtil();
    AuthLoginImpl authLoginImpl = new AuthLoginImpl();
    private CLogger log = CLogger.getCLogger(getClass());
    public Properties wsenv = new Properties();

    public Properties getDefaultCtx() {
        wsenv.setProperty("#AD_Language", "en_US");
        wsenv.setProperty("context", "deverp.semenindonesia.com/ws");
        return wsenv;
    }

    public ResponseData insertInvoiceVendor(ParamInvoice param, String token) {
        MInvoice i = null;
        String trxName = Trx.createTrxName("IV");
        Trx trx = Trx.get(trxName, true);

        ParamRequest param2 = new ParamRequest();
        param2.setToken(token);
        Map<String, Object> mToken =
                (Map<String, Object>) authLoginImpl.infoToken(param2).getResultdata();
        Integer ad_client_id = (Integer) mToken.get("ad_client_id");
        Integer ad_user_id = (Integer) mToken.get("ad_user_id");

        wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        Env.setCtx(wsenv);

        try {
             String hDesc = param.getDescription_header();
            // Integer C_DocTypeTarget_ID = Integer.valueOf(param.getC_DocTypeTarget_ID());
            // String dateInvoiced = param.getDate_invoiced();
             Integer C_BPartner_ID = Integer.valueOf(param.getC_BPartner_ID());
             Integer AD_Org_ID = Integer.valueOf((param.getAD_Org_ID()));
            // String dateAcct = param.getDateAcct();
            // Integer M_PriceList_ID = Integer.valueOf(param.getM_Price_List_ID());
            // Integer SalesRep_ID = Integer.valueOf(param.getSalesRep_ID());
            // String PaymentRule = param.getPayment_Rule();
            // Integer C_PaymentTerm_ID = Integer.valueOf(param.getC_Payment_Term_ID());
            // Integer C_BPartner_Location_ID = Integer.valueOf(param.getC_BPartner_Location_ID());
            List<ParamInvoiceLine> listLine = param.getLine_list();

            Integer line = 10;

            String whereClause;
            whereClause = " ISO_Code = 'IDR' ";
            MCurrency cur =
                    new Query(Env.getCtx(), MCurrency.Table_Name, whereClause, trxName).first();
            
            whereClause = " Name = 'AP Invoice' AND AD_Client_ID = ? ";
            MDocType docType = new Query(Env.getCtx(), MDocType.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();

            whereClause = " Name = 'Harga Beli Include' AND AD_Client_ID = ? ";
            MPriceList priceList =
                    new Query(Env.getCtx(), MPriceList.Table_Name, whereClause, trxName)
                            .setParameters(ad_client_id).first();

            whereClause = " Name = 'ssm26' AND AD_Client_ID = ? ";
            MUser uRep = new Query(Env.getCtx(), MUser.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();

            whereClause = " Name = 'VZ' AND AD_Client_ID = ? ";
            MTax tax = new Query(Env.getCtx(), MTax.Table_Name, whereClause, trxName)
                    .setParameters(ad_client_id).first();

            whereClause = " C_BPartner_ID = ? AND AD_Client_ID = ? ";
            MBPartnerLocation bpLoc = new Query(Env.getCtx(), MBPartnerLocation.Table_Name, whereClause, trxName)
                    .setParameters(Integer.valueOf(C_BPartner_ID),ad_client_id).first();
            
            
            // create header
            i = new MInvoice(Env.getCtx(), 0, trxName);
            i.setAD_Org_ID(AD_Org_ID);
            i.setIsActive(true);
            i.setC_DocTypeTarget_ID(docType.get_ID());
            i.setC_DocType_ID(docType.get_ID());
            i.setDateInvoiced(new Timestamp(new Date().getTime()));
            i.setC_BPartner_ID(Integer.valueOf(C_BPartner_ID));
            i.setC_BPartner_Location_ID(bpLoc.get_ID());
            i.setDateAcct(new Timestamp(new Date().getTime()));
            i.setM_PriceList_ID(priceList.get_ID());
            i.setC_Currency_ID(cur.get_ID());
            i.setPaymentRule(getPaymentRule(trxName));
            i.setAD_User_ID(ad_user_id);
            i.setSalesRep_ID(uRep.get_ID());
            i.setPOReference(hDesc);
            i.saveEx();

            for (ParamInvoiceLine pil : listLine) {

                // create detail
                MInvoiceLine il = new MInvoiceLine(Env.getCtx(), 0, trxName);
                il.setAD_Org_ID(AD_Org_ID);
                il.setC_Invoice_ID(i.getC_Invoice_ID());
                il.setLine(line);
                il.setC_Charge_ID(Integer.valueOf(pil.getC_Charge_ID()));
                il.setQtyEntered(Env.ONE);
                il.setQtyInvoiced(Env.ONE);
                il.setPriceEntered(new BigDecimal(pil.getPrice_Entered()));
                il.setPriceActual(new BigDecimal(pil.getPrice_Entered()));
                il.setC_Tax_ID(tax.get_ID());

                il.saveEx();
                line += 10;

            }

            MWorkflow.runDocumentActionWorkflow(i, DocAction.ACTION_Complete);
            resp.setCodestatus("S");
            resp.setMessage("Successed at ID: "
                    + String.valueOf(i.get_ID() + ", No Document: " + i.getDocumentNo()));
        } catch (AdempiereException e) {
            resp.setCodestatus("E");
            resp.setMessage("Failed caused by : " + e.getMessage());
            trx.rollback();
        } catch (Exception e) {
            resp.setCodestatus("E");
            resp.setMessage("Failed caused by : " + e.getMessage());
            trx.rollback();
        } finally {
            if (trx.isActive()) {
                trx.commit();
            }
            trx.close();
        }

        resp.setResultdata(null);
        return resp;
    }

//    public ResponseData actSetPartner(ParamRequest param) {
//        List<Object> listdata = new ArrayList<Object>();
//        try {
//            if (param.getC_bpartner_id().intValue() > 0) {
//                MBPartner bp =
//                        new MBPartner(Env.getCtx(), param.getC_bpartner_id().intValue(), null);
//
//                bp.setName(param.getName_partner());
//                bp.setC_BP_Group_ID(WebserviceUtil.defaultBPGroup(Env.getCtx()));
//                bp.setSalesVolume((int) param.getSalesvolume());
//                bp.setReferenceNo(param.getReferenceno());
//                bp.set_ValueOfColumn("crm_id", param.getCrm_id());
//                if (bp.save()) {
//                    String prmtr = "AD_Client_ID = " + bp.getAD_Client_ID() + " AND C_BPartner_ID="
//                            + bp.getC_BPartner_ID();
//                    Query q = new Query(Env.getCtx(), "C_BPartner_Location", prmtr, null);
//                    q.setOnlyActiveRecords(true);
//                    MBPartnerLocation bploc = (MBPartnerLocation) q.first();
//                    MLocation loc;
//                    if (bploc == null) {
//                        loc = new MLocation(Env.getCtx(), 0, null);
//                    } else {
//                        loc = new MLocation(Env.getCtx(), bploc.getC_Location_ID(), null);
//                    }
//                    loc.setAddress1(param.getAddress1());
//                    loc.setAddress2(param.getAddress2());
//                    loc.setAddress3(param.getAddress3());
//                    loc.setAddress4(param.getAddress4());
//                    loc.setC_Country_ID(param.getC_country_id().intValue());
//                    loc.setC_Region_ID(param.getC_region_id().intValue());
//                    loc.setC_City_ID(param.getC_city_id().intValue());
//                    if (loc.save()) {
//                        if (bploc == null) {
//                            MBPartnerLocation bplocation =
//                                    new MBPartnerLocation(Env.getCtx(), 0, null);
//                            bplocation.setName(param.getName_partner());
//                            bplocation.setC_BPartner_ID(bp.getC_BPartner_ID());
//                            bplocation.setC_Location_ID(loc.getC_Location_ID());
//                            bplocation.saveEx();
//                        }
//                        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                        xmap.put("c_bpartner_id", Integer.valueOf(bp.getC_BPartner_ID()));
//                        xmap.put("partner_value", bp.getValue());
//                        xmap.put("c_bpartner_location_id",
//                                Integer.valueOf(bploc.getC_BPartner_Location_ID()));
//                        xmap.put("c_location_id", Integer.valueOf(loc.getC_Location_ID()));
//                        xmap.put("crm_id", bp.get_Value("crm_id"));
//                        listdata.add(xmap);
//                        this.resp.setCodestatus("S");
//                        this.resp.setMessage("Success");
//                        this.resp.setResultdata(listdata);
//                    } else {
//                        this.resp.setMessage("Cannot save Location");
//                    }
//                }
//            } else {
//                MBPartner partner = new MBPartner(Env.getCtx(), 0, null);
//                partner.setName(param.getName_partner());
//                if (!Util.isEmpty(param.getValue_partner())) {
//                    partner.setValue(param.getValue_partner());
//                }
//                if (param.getC_bp_status_id() != null) {
//                    partner.set_ValueOfColumn("c_bp_status_id", param.getC_bp_status_id());
//                }
//                partner.setSalesVolume((int) param.getSalesvolume());
//
//                partner.setIsCustomer(true);
//                partner.set_ValueOfColumn("crm_id", param.getCrm_id());
//                if (partner.save()) {
//                    MLocation location = new MLocation(Env.getCtx(), 0, null);
//                    location.setAddress1(param.getAddress1());
//                    location.setAddress2(param.getAddress2());
//                    location.setAddress3(param.getAddress3());
//                    location.setAddress4(param.getAddress4());
//                    location.setC_Country_ID(param.getC_country_id().intValue());
//                    location.setC_Region_ID(param.getC_region_id().intValue());
//                    location.setC_City_ID(param.getC_city_id().intValue());
//                    if (location.save()) {
//                        MBPartnerLocation bpLoc = new MBPartnerLocation(Env.getCtx(), 0, null);
//                        bpLoc.setName(param.getName_partner());
//                        bpLoc.setC_BPartner_ID(partner.getC_BPartner_ID());
//                        bpLoc.setC_Location_ID(location.getC_Location_ID());
//                        bpLoc.saveEx();
//                        Map<String, Object> xmap = new LinkedHashMap<String, Object>();
//                        xmap.put("c_bpartner_id", Integer.valueOf(partner.getC_BPartner_ID()));
//                        xmap.put("partner_value", partner.getValue());
//                        xmap.put("c_bpartner_location_id",
//                                Integer.valueOf(bpLoc.getC_BPartner_Location_ID()));
//                        xmap.put("c_location_id", Integer.valueOf(location.getC_Location_ID()));
//                        xmap.put("crm_id", partner.get_Value("crm_id"));
//
//                        listdata.add(xmap);
//                        this.resp.setCodestatus("S");
//                        this.resp.setMessage("Success");
//                        this.resp.setResultdata(listdata);
//                    } else {
//                        this.resp.setMessage("Cannot save Location Partner");
//                    }
//                } else {
//                    this.resp.setMessage("Cannot save Customer");
//                }
//            }
//        } catch (Exception e) {
//            this.resp.setCodestatus("E");
//            this.resp.setMessage(e.getMessage());
//        }
//        return this.resp;
//    }

    // SQL
    private String getPaymentRule(String trxName) {
        String result = "";

        // @formatter:off
        String sql = 
                "select " + 
                "    b.ad_ref_list_id " + 
                "from ad_reference a " + 
                "inner join ad_ref_list b on b.ad_reference_id = a.ad_reference_id " + 
                "where lower(a.name) like lower('%_Payment Rule%') " + 
                "and lower(b.name) like lower('%Check%')"
                ;
        // @formatter:on

        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql, trxName);
            rs = ps.executeQuery();

            while (rs.next()) {
                result = rs.getString("ad_ref_list_id");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, sql, e);
        } finally {
            DB.close(rs, ps);
            rs = null;
            ps = null;
        }
        return result;
    }

}

