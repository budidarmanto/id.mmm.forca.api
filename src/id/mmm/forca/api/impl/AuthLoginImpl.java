package id.mmm.forca.api.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.compiere.model.MSession;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;
import id.mmm.forca.api.filter.WsLogin;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.sisi.forca.authenticationapi.auth.AuthenticationApi;
import id.sisi.forca.authenticationapi.model.MFORCAUserMobile;
import id.sisi.forca.authenticationapi.model.MFORCAWSToken;
import id.sisi.forca.authenticationapi.response.ResponseData;

public class AuthLoginImpl {
    private CLogger wslog = CLogger.getCLogger(getClass());
    public static Properties wsenv = new Properties();
    public List<Object> listdata = new ArrayList<Object>();
    ResponseData resp = new ResponseData();

    public static Properties getDefaultCtx() {
        wsenv.setProperty("#AD_Language", "en_US");
        wsenv.setProperty("context", "deverp.semenindonesia.com");
        wsenv.setProperty("#AD_CLIENT_ID", "0");
        return wsenv;
    }

    // Login forca
    @SuppressWarnings("unchecked")
    public ResponseData getLoginForca(String user, String password) {
        getDefaultCtx();

        if (user.isEmpty() || user == null || password.isEmpty() || password == null) {
            resp.setMessage("Username or Password required");
            return resp;
        }

        KeyNamePair[] clients = null;

        WsLogin login = new WsLogin(wsenv);
        clients = login.getClients(user, password);

        StringBuilder errorMessage = new StringBuilder();
        Map<String, String> map = new LinkedHashMap<String, String>();
        if (clients == null) {
            resp.setMessage("Username or Password inconsistent");
            return resp;

        } else {
            wsenv.setProperty(Constants.CTX_CLIENT, clients[0].getID());
            MUser mUser = MUser.get(wsenv, user);
            if (mUser != null) {

                // param get token
                ParamRequest param = new ParamRequest();
                param.setUsername(user);
                param.setAd_user_id(mUser.getAD_User_ID());
                param.setPassword(password);
                param.setAd_client_id(mUser.getAD_Client_ID());

                try {
                    Integer ad_role_id = 0;
                    Integer a = 0;
                    List<Object> respRole = (List<Object>) getRoleForca(param).getResultdata();
                    if (respRole.size() > 1) {
                        while (ad_role_id == 0) {
                            Map<String, String> mWh = (Map<String, String>) respRole.get(a);
                            ad_role_id = Integer.valueOf((String) mWh.get("ad_role_id"));
                            a += 1;
                        }
                    } else {
                        Map<String, String> mWh = (Map<String, String>) respRole.get(a);
                        ad_role_id = Integer.valueOf((String) mWh.get("ad_role_id"));
                    }
                    param.setAd_role_id(ad_role_id);
                } catch (Exception e) {
                    errorMessage.append("Failed to get ad_role_id \n ");
                }

                Integer ad_org_id = 0;
                try {
                    Integer c = 0;
                    List<Object> respOrg = (List<Object>) getOrgForca(param).getResultdata();
                    if (respOrg.size() > 1) {
                        while (ad_org_id == 0) {
                            Map<String, String> mWh = (Map<String, String>) respOrg.get(c);
                            ad_org_id = Integer.valueOf((String) mWh.get("ad_org_id"));
                            c += 1;
                        }
                    } else {
                        Map<String, String> mWh = (Map<String, String>) respOrg.get(c);
                        ad_org_id = Integer.valueOf((String) mWh.get("ad_org_id"));
                    }

                    param.setAd_org_id(ad_org_id);
                } catch (Exception e) {
                    errorMessage.append("Failed to get ad_org_id \n ");
                }


                try {
                    Integer m_warehouse_id = 0;
                    Integer b = 0;
                    List<Object> respWh = (List<Object>) getWarehouseForca(param).getResultdata();
                    if (respWh.size() != 0) {
                        while (m_warehouse_id == 0) {
                            Map<String, String> mWh = (Map<String, String>) respWh.get(b);
                            m_warehouse_id = Integer.valueOf((String) mWh.get("m_warehouse_id"));
                            b += 1;
                        }
                    } else {
                        Map<String, String> mWh = (Map<String, String>) respWh.get(b);
                        m_warehouse_id = Integer.valueOf((String) mWh.get("m_warehouse_id"));
                    }
                    param.setM_warehouse_id(m_warehouse_id);
                } catch (Exception e) {
                    errorMessage.append("Failed to get m_warehouse_id \n ");
                }

                map.put("ad_user_id", String.valueOf(mUser.getAD_User_ID()));
                map.put("ad_user_name", String.valueOf(mUser.getName()));
                map.put("ad_client_id", String.valueOf(mUser.getAD_Client_ID()));
                map.put("ad_org_id", String.valueOf(ad_org_id));
                map.put("salesrep_ID", String.valueOf(mUser.getAD_User_ID()));

                try {
                    Map<String, Object> mToken =
                            (Map<String, Object>) getTokenApi(param).getResultdata();
                    map.put("token", (String) mToken.get("token"));
                } catch (Exception e) {
                    errorMessage.append("Failed to get Token \n ");
                }

            }

            if (errorMessage.length() == 0) {
                resp.setCodestatus("S");
                resp.setMessage("Data found");
                resp.setResultdata(map);
            } else {
                resp.setCodestatus("E");
                resp.setMessage(errorMessage.toString());
                resp.setResultdata(new ArrayList<>());
            }

            return resp;
        }
    }
    
  //get User Forca
    public ResponseData getListUserClientForca() {
        List<Object> listData = new ArrayList<Object>();
        String sql = 
                "select " + 
                "    a.ad_client_id, " + 
                "    a.ad_user_id, " + 
                "    b.ad_role_id, " + 
                "    a.password, " + 
                "    a.name, " + 
                "    a.email, " + 
                "    a.phone, " + 
                "    a.islocked, " + 
                "    a.phone2, " + 
                "    a.description " + 
                "from ad_user a " + 
                "inner join ad_user_roles b on b.ad_user_id = a.ad_user_id and b.ad_client_id = a.ad_client_id " + 
                "where a.password is not null " + 
                "and a.isuserclient = 'Y' " 
                ;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql, null);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                Map<String,String> xmap = new LinkedHashMap<String, String>();
                xmap.put("ad_client_id", rs.getString("ad_client_id"));
                xmap.put("ad_user_id", rs.getString("ad_user_id"));
                xmap.put("ad_role_id", rs.getString("ad_role_id"));
                xmap.put("password", rs.getString("password"));
                xmap.put("name", rs.getString("name"));
                xmap.put("email", rs.getString("email"));
                xmap.put("phone", rs.getString("phone"));
                xmap.put("islocked", rs.getString("islocked"));
                xmap.put("phone2", rs.getString("phone2"));
                xmap.put("description", rs.getString("description"));
                listData.add(xmap);
            }
            resp.setCodestatus("S");
            resp.setMessage("Data Founded");
            resp.setResultdata(listData);
        } catch (Exception e) {
            resp.setCodestatus("E");
            resp.setMessage("Can't get data User Forca !");
            resp.setResultdata(null);
        } finally {
            DB.close(rs);
            rs=null;
            ps=null;
        }
        
        return resp;
    }

 // get Last Login
    public ResponseData getLastLogin(ParamRequest param) {
        List<Object> listData = new ArrayList<Object>();
        // Get the number of days in that month
        String tahun = param.getPeriode().substring(0, 4);
        String bulan = param.getPeriode().substring(4, 6);
        YearMonth yearMonthObject = YearMonth.of(Integer.valueOf(tahun),
                Integer.valueOf(bulan));
        int daysInMonth = yearMonthObject.lengthOfMonth();
        StringBuilder sql = new StringBuilder();
        StringBuilder select = new StringBuilder();
        StringBuilder join = new StringBuilder();

        //@formatter:off
        sql.append(
                "   with ct as(         " + 
                        "        select " + 
                        "            name " + 
                        "            ,day " + 
                        "            ,tgl " + 
                        "            ,updated " + 
                        "         from ( " + 
                        "             select " + 
                        "                row_number()over(partition by day,name,tgl order by a.updated desc) as row, a.* " + 
                        "            from ( " + 
                        "                 select " + 
                        "                    date_part('day',b.updated) as day, " + 
                        "                    a.name, " + 
                        "                    to_char(b.updated,'YYYYMM') tgl, " + 
                        "                    b.updated " + 
                        "                 from ad_user a " + 
                        "                 inner join ad_session b on b.createdby = a.ad_user_id " + 
                        "                 where  to_char(b.updated,'YYYYMM') = ?  " + 
                        "                 and a.ad_client_id = ? " + 
                        "             ) a " + 
                        "         ) as sub " + 
                        "         where row = 1 " + 
                        "    ) ");
        
        select.append(" select distinct e.name ");
        join.append(" FROM (SELECT DISTINCT name, tgl,updated FROM ct) e ");
        for(int a = 1; a<=daysInMonth;a++) {
            select.append(" , d"+a+".updated AS day"+a);
            join.append(" LEFT JOIN (SELECT name, updated,tgl FROM ct WHERE day = "+a+" and tgl = ?) d"+a+" USING(name,tgl) ");
        }
        sql.append(select.toString());
        sql.append(join.toString());
        sql.append(" ORDER BY e.name asc; ");     
        
        //@formatter:on 
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = DB.prepareStatement(sql.toString(), null);
            ps.setString(1, param.getPeriode());
            ps.setInt(2, Env.getAD_Client_ID(Env.getCtx()));
            for (int a = 1; a <= daysInMonth; a++) {
                ps.setString(a+2, param.getPeriode());
            }

            rs = ps.executeQuery();
            while (rs.next()) {
                Map<String, String> xmap = new LinkedHashMap<String, String>();
                xmap.put("name", rs.getString(1));
                for (int a = 1; a <= daysInMonth; a++) {
                    xmap.put("day" + a, rs.getString(a + 1));
                }
                listData.add(xmap);
            }
            resp.setCodestatus("S");
            resp.setMessage(String.valueOf(listData.size()) + " Data Found");
            resp.setResultdata(listData);
        } catch (Exception e) {
            resp.setCodestatus("E");
            resp.setMessage("Can't get data User Forca !");
            resp.setResultdata(null);
        } finally {
            DB.close(rs);
            rs = null;
            ps = null;
        }
        return resp;
    }

    
    public ResponseData getLogout(ParamRequest param) {
        String token = param.getToken();
        String whereCond = "FORCA_WS_Token_UU = ?";
        MFORCAWSToken wt = new Query(Env.getCtx(), MFORCAWSToken.Table_Name, whereCond, null)
                .setParameters(token).first();
        wt.setIsActive(false);
        wt.saveEx();
        resp.setCodestatus("S");
        resp.setMessage("Logout Success");
        return resp;
    }

    public ResponseData insertUserMobile(ParamRequest param) {
        Map<String, Object> mToken = (Map<String, Object>) infoToken(param).getResultdata();
        Timestamp exptime = new Timestamp(System.currentTimeMillis());
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(exptime.getTime());
        cal.add(Calendar.MONTH, 1);// expired 1 bulan
        exptime = new Timestamp(cal.getTime().getTime());

        MFORCAUserMobile usermobile = new MFORCAUserMobile(Env.getCtx(), 0, null);
        usermobile.setAD_Org_ID((Integer) mToken.get("ad_user_id"));
        usermobile.setAD_Role_ID((Integer) mToken.get("ad_role_id"));
        usermobile.setAD_User_ID((Integer) mToken.get("ad_user_id"));
        usermobile.setM_Warehouse_ID((Integer) mToken.get("m_warehouse_id"));
        usermobile.setExpiredDate(exptime);
        usermobile.setFORCA_Token(param.getToken());
        usermobile.setFORCA_Platform(param.getPlatform());
        usermobile.setFORCA_Version(param.getVersion());
        usermobile.setFORCA_Device_ID(param.getDevice_id());
        usermobile.setFORCA_Model(param.getModel());
        usermobile.setFORCA_Manufaktur(param.getManufaktur());
        usermobile.saveEx();
        resp.setCodestatus("S");
        resp.setMessage("Data saved to user mobile");

        return resp;
    }

    public ResponseData getClientForca(String user, String password) {
        try {
            if (user.equals("") || user == null || password.equals("") || password == null) {
                resp.setMessage("Username or Password required");
                return resp;
            }

            KeyNamePair[] clients = null;

            WsLogin login = new WsLogin(wsenv);

            clients = login.getClients(user, password);
            if (clients == null) {
                // resp.setCodestatus("E");
                resp.setMessage("Username and Password inconsistent");
                return resp;
            } else {
                Map<String, String> map = new LinkedHashMap<String, String>();
                // for(int i=0; i<clients.length; i++){
                map.put("ad_client_id", clients[0].getID());
                map.put("client_name", clients[0].getName());
                // }
                resp.setCodestatus("S");
                resp.setMessage("Data found");
                resp.setResultdata(map);

                return resp;
            }
        } catch (Exception e) {
            resp.setMessage(e.getMessage());
            return resp;
        }
    }

    public ResponseData getRoleForca(ParamRequest param) {
        String user = param.getUsername();
        String password = param.getPassword();
        Integer ad_client_id = param.getAd_client_id();
        // Integer ad_user_id = param.getAd_user_id();


        if (!DB.isConnected()) {
            // resp.setCodestatus("E");
            resp.setMessage("No Database Connection");
            return resp;
        }
        // || ad_user_id == null
        if (user == null // || password == null
                || ad_client_id == null) {
            // resp.setCodestatus("E");
            resp.setMessage("Parameter required");
            return resp;
        }

        KeyNamePair clients = new KeyNamePair(ad_client_id, user);
        KeyNamePair[] roles = null;
        getDefaultCtx();
        // wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));

        WsLogin login = new WsLogin(wsenv);

        // clients = login.getClients (user,password);

        roles = login.getRoles(user, clients);

        if (roles == null) {
            resp.setMessage("Roles not found for this user");
            return resp;
        } else {
            // roles = login.getRoles(APP_USER, clients[0]);

            List<Object> resultdata = new ArrayList<Object>();

            for (int i = 0; i < roles.length; i++) {
                Map<String, String> map = new LinkedHashMap<String, String>();
                map.put("ad_role_id", roles[i].getID());
                map.put("role_name", roles[i].getName());
                resultdata.add(map);
            }

            // setUserID(wsc.ctx, clients[0].getKey());
            resp.setCodestatus("S");
            resp.setMessage("Data found");
            resp.setResultdata(resultdata);
            return resp;
        }
    }

    public ResponseData getOrgForca(ParamRequest param) {
        ResponseData resp = new ResponseData();
        String user = param.getUsername();
        // String password = param.getPassword();
        Integer ad_client_id = param.getAd_client_id();
        // Integer ad_user_id = param.getAd_user_id();
        Integer ad_role_id = param.getAd_role_id();


        if (!DB.isConnected()) {
            // resp.setCodestatus("E");
            resp.setMessage("No Database Connection");
            return resp;
        }
        if (user == null // || password == null || ad_user_id == null
                || ad_client_id == null || ad_role_id == null) {
            // resp.setCodestatus("E");
            resp.setMessage("Parameter required");
            return resp;
        }

        // KeyNamePair clients = new KeyNamePair(ad_client_id, "Nama Client");
        KeyNamePair roles = new KeyNamePair(ad_role_id, "");
        KeyNamePair[] orgs = null;
        getDefaultCtx();
        // wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
        wsenv.setProperty(Constants.CTX_ROLE, String.valueOf(ad_role_id));

        WsLogin login = new WsLogin(wsenv);
        orgs = login.getOrgs(roles);

        if (orgs == null) {
            // resp.setCodestatus("E");
            resp.setMessage("Organization not found for this role");

            return resp;
        } else {
            // roles = login.getRoles(APP_USER, clients[0]);

            List<Object> resultdata = new ArrayList<Object>();
            for (int i = 0; i < orgs.length; i++) {
                Map<String, String> map = new LinkedHashMap<String, String>();
                map.put("ad_org_id", orgs[i].getID());
                map.put("org_name", orgs[i].getName());
                resultdata.add(map);
            }


            resp.setCodestatus("S");
            resp.setMessage("Data found");
            resp.setResultdata(resultdata);
            return resp;
        }
    }

    public ResponseData getWarehouseForca(ParamRequest param) {
        // ResponseData resp = new ResponseData();
        String user = param.getUsername();
        // String password = param.getPassword();
        Integer ad_client_id = param.getAd_client_id();
        // Integer ad_user_id = param.getAd_user_id();
        // Integer ad_role_id = param.getAd_role_id();
        Integer ad_org_id = param.getAd_org_id();


        if (!DB.isConnected()) {
            // resp.setCodestatus("E");
            resp.setMessage("No Database Connection");
            return resp;
        }
        if (user == null // || password == null || ad_user_id == null
                || ad_client_id == null || ad_org_id == null) {
            // resp.setCodestatus("E");
            resp.setMessage("Parameter required");
            return resp;
        }

        // KeyNamePair clients = new KeyNamePair(ad_client_id, "Nama Client");
        // KeyNamePair roles = new KeyNamePair(ad_role_id, "Nama Role");
        KeyNamePair[] whouse = null;

        getDefaultCtx();
        // wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
        wsenv.setProperty(Constants.CTX_ORG, String.valueOf(ad_org_id));
        Env.setCtx(wsenv);

        WsLogin login = new WsLogin(wsenv);
        whouse = login.getWarehouses(new KeyNamePair(ad_org_id, ""));

        if (whouse == null) {
            // resp.setCodestatus("E");
            resp.setMessage("Organization not found for this role");
            return resp;
        } else {
            // roles = login.getRoles(APP_USER, clients[0]);

            List<Object> resultdata = new ArrayList<Object>();

            for (int i = 0; i < whouse.length; i++) {
                Map<String, String> map = new LinkedHashMap<String, String>();
                map.put("m_warehouse_id", whouse[i].getID());
                map.put("warehouse_name", whouse[i].getName());
                resultdata.add(map);
            }


            resp.setCodestatus("S");
            resp.setMessage("Data found");
            resp.setResultdata(resultdata);
            return resp;
        }
    }

    public ResponseData getTokenApi(ParamRequest param) {

        String username = param.getUsername();
        String password = param.getPassword();
        Integer ad_client_id = param.getAd_client_id();
        // Integer ad_user_id = param.getAd_user_id();
        Integer ad_role_id = param.getAd_role_id();
        Integer ad_org_id = param.getAd_org_id();
        Integer m_warehouse_id = param.getM_warehouse_id();
        Integer c_bpartner_id = param.getC_bpartner_id();
        KeyNamePair[] users = null;

        if (!DB.isConnected()) {
            resp.setMessage("No Database Connection");
            return resp;
        }
        if (username == null || password == null // || ad_user_id == null
                || ad_client_id == null || ad_org_id == null || ad_role_id == null) {
            resp.setMessage("Parameter required");
            return resp;
        }
        try {
            Timestamp exptime = new Timestamp(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(exptime.getTime());
            cal.add(Calendar.MONTH, 1);
            exptime = new Timestamp(cal.getTime().getTime());

            WsLogin login = new WsLogin(getDefaultCtx());
            users = login.getAD_User(username, password);
            AuthenticationApi.setContext(Integer.parseInt(users[0].getID()), ad_client_id,
                    ad_role_id, ad_org_id, m_warehouse_id);

            MFORCAWSToken wstoken = new MFORCAWSToken(Env.getCtx(), 0, null);
            wstoken.setAD_Org_ID(ad_org_id);
            wstoken.setAD_Role_ID(ad_role_id);
            wstoken.setAD_User_ID(Integer.parseInt(users[0].getID()));
            wstoken.setM_Warehouse_ID(m_warehouse_id);
            // wstoken.setC_BPartner_ID(c_bpartner_id);
            wstoken.setExpiredDate(exptime); // expired 1 bulan
            if (wstoken.save()) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("token", wstoken.getFORCA_WS_Token_UU());
                map.put("ad_user_id", users[0].getID());
                map.put("expired", wstoken.getExpiredDate().toString());
                resp.setCodestatus("S");
                resp.setMessage("Berhasil");
                resp.setResultdata(map);
            } else {
                resp.setMessage("Cannot Create Token, Check Parameter Require");
            }


        } catch (Exception e) {
            resp.setMessage(e.getMessage());
        }

        // KeyNamePair clients = new KeyNamePair(ad_client_id, "Nama Client");
        // KeyNamePair roles = new KeyNamePair(ad_role_id, "Nama Role");
        // KeyNamePair[] whouse = null;
        //
        // getDefaultCtx();
        // //wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
        // wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
        // wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
        // wsenv.setProperty(Constants.CTX_ORG, String.valueOf(ad_org_id));
        // Env.setCtx(wsenv);
        //
        // WsLogin login = new WsLogin(wsenv);
        // whouse = login.getWarehouses(new KeyNamePair(ad_org_id, ""));
        //
        // if (whouse == null){
        // //resp.setCodestatus("E");
        // resp.setMessage("Organization not found for this role");
        // return resp;
        // }else{
        // //roles = login.getRoles(APP_USER, clients[0]);
        //
        // List<Object> resultdata = new ArrayList<Object>();
        // Map<String, String> map = new LinkedHashMap<String, String>();
        // for(int i=0; i<whouse.length; i++){
        // map.put(whouse[i].getID(), whouse[i].getName());
        // }
        // resultdata.add(map);
        //
        // resp.setCodestatus("S");
        // resp.setMessage("Data found");
        // resp.setResultdata(resultdata);
        //
        // }
        return resp;
    }

    public ResponseData refreshToken(ParamRequest param) {

        String token = param.getToken();

        if (!DB.isConnected()) {
            resp.setMessage("No Database Connection");
            return resp;
        }
        if (token == null) {
            resp.setMessage("Parameter required");
            return resp;
        }
        try {
            Timestamp exptime = new Timestamp(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(exptime.getTime());
            cal.add(Calendar.MONTH, 1);
            exptime = new Timestamp(cal.getTime().getTime());

            AuthenticationApi.setContextByToken(token);

            String prmtr = "FORCA_WS_Token_UU = '" + token + "' ";
            Query q = new Query(getDefaultCtx(), MFORCAWSToken.Table_Name, prmtr, null);
            q.setOnlyActiveRecords(true);
            MFORCAWSToken MFORCAWSToken = q.first();

            if (MFORCAWSToken.getFORCA_WS_Token_ID() > 0) {
                MFORCAWSToken newtoken = new MFORCAWSToken(Env.getCtx(), 0, null);
                newtoken.setAD_Org_ID(MFORCAWSToken.getAD_Client_ID());
                newtoken.setAD_Role_ID(MFORCAWSToken.getAD_Role_ID());
                newtoken.setAD_User_ID(MFORCAWSToken.getAD_User_ID());
                newtoken.setM_Warehouse_ID(MFORCAWSToken.getM_Warehouse_ID());
                newtoken.setExpiredDate(exptime); // expired 1 bulan
                if (newtoken.save()) {
                    Map<String, Object> map = new LinkedHashMap<String, Object>();
                    map.put("token", newtoken.getFORCA_WS_Token_UU());
                    map.put("ad_client_id", newtoken.getAD_Client_ID());
                    map.put("ad_role_id", newtoken.getAD_Role_ID());
                    map.put("ad_org_id", newtoken.getAD_Org_ID());
                    map.put("ad_user_id", newtoken.getAD_User_ID());
                    map.put("expired", newtoken.getExpiredDate());
                    resp.setCodestatus("S");
                    resp.setMessage("Berhasil");
                    resp.setResultdata(map);
                } else {
                    resp.setMessage("Cannot Create Token, Check Parameter Require");
                }
            } else {
                resp.setMessage("No Valid Token");
            }
        } catch (Exception e) {
            resp.setMessage(e.getMessage());
        }
        return resp;
    }

    public ResponseData infoToken(ParamRequest param) {

        String token = param.getToken();
        if (!DB.isConnected()) {
            resp.setMessage("No Database Connection");
            return resp;
        }
        if (token == null) {
            resp.setMessage("Parameter required");
            return resp;
        }
        try {
            AuthenticationApi.setContextByToken(token);

            String prmtr = "FORCA_WS_Token_UU = '" + token + "' ";
            Query q = new Query(Env.getCtx(), MFORCAWSToken.Table_Name, prmtr, null);
            q.setOnlyActiveRecords(true);

            // MFORCAUserMobile m = new MFORCAUserMobile(Env.getCtx(), 0, null);

            MFORCAWSToken MFORCAWSToken = q.first();

            // MFORCAWSToken mw = new MFORCAWSToken(ctx, rs, trxName);

            if (MFORCAWSToken != null) {
                Map<String, Object> map = new LinkedHashMap<String, Object>();
                map.put("token", MFORCAWSToken.getFORCA_WS_Token_UU());
                map.put("ad_client_id", MFORCAWSToken.getAD_Client_ID());
                map.put("ad_role_id", MFORCAWSToken.getAD_Role_ID());
                map.put("ad_org_id", MFORCAWSToken.getAD_Org_ID());
                map.put("ad_user_id", MFORCAWSToken.getAD_User_ID());
                map.put("m_warehouse_id", MFORCAWSToken.getM_Warehouse_ID());
                map.put("expired", MFORCAWSToken.getExpiredDate());
                map.put("hit", MFORCAWSToken.getHit());
                map.put("success", MFORCAWSToken.getSuccess());
                map.put("error", MFORCAWSToken.getErrorRequest());
                resp.setCodestatus("S");
                resp.setMessage("Berhasil");
                resp.setResultdata(map);
            } else {
                resp.setMessage("Cannot Find Token");
            }
        } catch (Exception e) {
            resp.setMessage(e.getMessage());
        }
        return resp;
    }

    public static void sesionLogout() {
        MSession sesion = MSession.get(Env.getCtx(), false);
        sesion.logout();
    }
}
