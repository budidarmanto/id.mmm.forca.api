package id.mmm.forca.api.app;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.core.Application;
import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
import com.wordnik.swagger.model.ApiInfo;
import com.wordnik.swagger.reader.ClassReaders;
import id.mmm.forca.api.app.path.Authentication_v3;
import id.mmm.forca.api.app.path.ForcaApprovalService_v3;
import id.mmm.forca.api.app.path.ForcaCrmCorma_v1;
import id.mmm.forca.api.app.path.ForcaInvoice_v1;
import id.mmm.forca.api.app.path.ForcaMaster_v1;
import id.mmm.forca.api.app.path.ForcaOrder_v1;

// @javax.ws.rs.ApplicationPath("api")
public class ServerAplication extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();

        // swagger Core Plugin
        s.add(com.wordnik.swagger.jaxrs.listing.ApiListingResource.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ResourceListingProvider.class);


        addResourcePath(s);
        swaggerConfiguration();

        return s;
    }

    private void addResourcePath(Set<Class<?>> resource) {
        resource.add(Authentication_v3.class);
        resource.add(ForcaApprovalService_v3.class);
        resource.add(ForcaInvoice_v1.class);
        resource.add(ForcaOrder_v1.class);
        resource.add(ForcaCrmCorma_v1.class);
        resource.add(ForcaMaster_v1.class);
    }

    private void swaggerConfiguration() {
        SwaggerConfig swaggerConfig = new SwaggerConfig();
        ConfigFactory.setConfig(swaggerConfig);

        swaggerConfig.setBasePath("/api/ws");
        ApiInfo info = new ApiInfo("Forca API Documentation",
                "RESTful API from Forca ERP<br>"
                        + "Please Contact administrator if you want to access it !!!",
                "http://sisi.id/", "administrator@forca.id", "Apache 2.0",
                "http://www.apache.org/licenses/LICENSE-2.0.html");
        swaggerConfig.setApiVersion("0.0.1");

        swaggerConfig.setApiInfo(info);

        // swaggerConfig.setSwaggerVersion("2.0"); define manual
        ScannerFactory.setScanner(new DefaultJaxrsScanner());
        ClassReaders.setReader(new DefaultJaxrsApiReader());
    }
}
