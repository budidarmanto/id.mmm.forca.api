package id.mmm.forca.api.app.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.compiere.util.Util;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.Authorization;
import id.mmm.forca.api.filter.RestFilter;
import id.mmm.forca.api.impl.AuthLoginImpl;
import id.mmm.forca.api.impl.ForcaApprovalImpl;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.auth.AuthenticationApi;
import id.sisi.forca.authenticationapi.response.ResponseData;

@Component
@Api(position = 3, value = "Forca Approval v3", description = "Forca approval API for mobile")
@Path("approval/v3")
public class ForcaApprovalService_v3 {
    WebserviceUtil util = new WebserviceUtil();
    ParamRequest param = new ParamRequest();
    AuthenticationApi authAPI = new AuthenticationApi();

    @GET
    @Path("/xmltest")
    @Produces(MediaType.TEXT_XML)
    public String sayHello() {
        return "<?xml version=\"1.0\"?>" + "<say><hello> Web Service" + "</hello></say>";
    }

    // User Logout
    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 1, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Login", authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getLogout() {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                AuthLoginImpl signIn = new AuthLoginImpl();
                result = signIn.getLogout(param);
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/insertusermobile")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "List All Pending Approval",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response insertUserMobile(
            @ApiParam(value = "Platform", required = true) @FormParam("platform") String platform,
            @ApiParam(value = "Device ID",
                    required = true) @FormParam("device_id") String device_id,
            @ApiParam(value = "Model", required = true) @FormParam("model") String model,
            @ApiParam(value = "Manufaktur",
                    required = true) @FormParam("manufaktur") String manufaktur,
            @ApiParam(value = "Version", required = true) @FormParam("version") String version) {

        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else if (Util.isEmpty(platform)) {
                result = util.resultResponse("E", "Please input parameter 'platform' ", null);
            } else if (Util.isEmpty(device_id)) {
                result = util.resultResponse("E", "Please input parameter 'device_id' ", null);
            } else if (Util.isEmpty(model)) {
                result = util.resultResponse("E", "Please input parameter 'model' ", null);
            } else if (Util.isEmpty(manufaktur)) {
                result = util.resultResponse("E", "Please input parameter 'manufaktur' ", null);
            } else if (Util.isEmpty(version)) {
                result = util.resultResponse("E", "Please input parameter 'version' ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setPlatform(platform);
                param.setDevice_id(device_id);
                param.setModel(model);
                param.setManufaktur(manufaktur);
                param.setVersion(version);
                AuthLoginImpl signIn = new AuthLoginImpl();
                result = signIn.insertUserMobile(param);
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }



    @POST
    @Path("/workflow")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 3, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "List All Pending Approval",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWorkflow() {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.getWorkflow(param, "All");
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/workflow/group")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 4, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Document Group Approval",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWorkflowGrup() {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.getWorkflow(param, "Group");
            }
        }

        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/workflow/multiexec")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 5, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Execute Multi Approval",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWFMultiExe(
            @ApiParam(value = "ad_wf_activity_id",
                    required = true) @FormParam("ad_wf_activity_id") String ad_wf_activity_id,
            @ApiParam(value = "message", required = true) @FormParam("message") String message,
            @ApiParam(value = "yesno", required = true) @FormParam("yesno") String yesno) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else if (Util.isEmpty(ad_wf_activity_id)) {
                result = util.resultResponse("E", "Please input parameter 'ad_wf_activity_id' ",
                        null);
            } else if (Util.isEmpty(message)) {
                result = util.resultResponse("E", "Please input parameter 'message' ", null);
            } else if (Util.isEmpty(yesno)) {
                result = util.resultResponse("E", "Please input parameter 'yesno' ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setAd_wf_activity_id(ad_wf_activity_id);
                param.setMessage(message);
                param.setYesno(yesno);

                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.approveMultiDokumen(param);
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/workflow/list")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 6, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Workflow List", authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWorkflowList(
            @ApiParam(value = "ad_wf_name",
                    required = true) @FormParam("ad_wf_name") String ad_wf_name,
            @ApiParam(value = "ad_wf_node_id",
                    required = true) @FormParam("ad_wf_node_id") String ad_wf_node_id) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else if (Util.isEmpty(ad_wf_name)) {
                result = util.resultResponse("E", "Please input parameter 'ad_wf_name' ", null);
            } else if (Util.isEmpty(ad_wf_node_id)) {
                result = util.resultResponse("E", "Please input parameter 'ad_wf_node_id' ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setAd_wf_name(ad_wf_name);
                param.setAd_wf_node_id(ad_wf_node_id);
                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.getWorkflow(param, "List");
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/workflow/detail")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 7, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Workflow List", authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWorkflowDetail(
            @ApiParam(value = "Workflow Name",
                    required = true) @FormParam("ad_wf_name") String ad_wf_name,
            @ApiParam(value = "Workflow Activity Id",
                    required = true) @FormParam("ad_wf_activity_id") String ad_wf_activity_id) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else if (Util.isEmpty(ad_wf_name)) {
                result = util.resultResponse("E", "Please input parameter 'ad_wf_name' ", null);
            } else if (Util.isEmpty(ad_wf_activity_id)) {
                result = util.resultResponse("E", "Please input parameter 'ad_wf_activity_id' ",
                        null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setAd_wf_name(ad_wf_name);
                param.setAd_wf_activity_id(ad_wf_activity_id);
                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.getWorkflow(param, "Detail");
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/workflow/approved")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 8, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Document Group Approval",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWfApproved(
            @ApiParam(value = "Date From (DD/MM/YYYY)",
                    required = true) @FormParam("x_tgl_from") String xtglfrom,
            @ApiParam(value = "Date To (DD/MM/YYYY)",
                    required = true) @FormParam("xtglto") String xtglto,
            @ApiParam(value = "Status", required = true) @FormParam("status") String status) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else if (Util.isEmpty(xtglfrom)) {
                result = util.resultResponse("E", "Please input parameter 'xtglfrom' ", null);
            } else if (Util.isEmpty(xtglto)) {
                result = util.resultResponse("E", "Please input parameter 'xtglto' ", null);
            } else if (Util.isEmpty(status)) {
                result = util.resultResponse("E", "Please input parameter 'status' ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setXtglfrom(xtglfrom);
                param.setXtglto(xtglto);
                param.setStatus(status);

                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.DokumenApproved(param, "Header");
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/workflow/approved/detail")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces("application/json")
    @ApiOperation(position = 8, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Document Group Approval",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getWfApprovedDetail(@ApiParam(value = "ad_wf_activity_id",
            required = true) @FormParam("ad_wf_activity_id") String ad_wf_activity_id) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! " + Constants.AUTH_FORCA_TOKEN,
                        null);
            } else if (Util.isEmpty(ad_wf_activity_id)) {
                result = util.resultResponse("E", "Please input parameter 'ad_wf_activity_id' ",
                        null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setAd_wf_activity_id(ad_wf_activity_id);

                ForcaApprovalImpl wf = new ForcaApprovalImpl();
                result = wf.DokumenApproved(param, "Detail");
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }


}

