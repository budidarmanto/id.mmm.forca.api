package id.mmm.forca.api.app.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.compiere.util.Util;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.Authorization;
import id.mmm.forca.api.filter.RestFilter;
import id.mmm.forca.api.impl.AuthLoginImpl;
import id.mmm.forca.api.impl.ForcaInvoiceImpl;
import id.mmm.forca.api.request.ParamInvoice;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.auth.AuthenticationApi;
import id.sisi.forca.authenticationapi.response.ResponseData;

@Component
@Api(position = 2, value = "FORCA Invoice v1", description = "Integration App HR Odoo")
@Path("invoice/v1")
public class ForcaInvoice_v1 {
    WebserviceUtil util = new WebserviceUtil();
    ParamRequest param = new ParamRequest();
    AuthenticationApi authAPI = new AuthenticationApi();

    @GET
    @Path("/xmltest")
    @Produces(MediaType.TEXT_XML)
    public String sayHello() {
        return "<?xml version=\"1.0\"?>" + "<say><hello> Web Service" + "</hello></say>";
    }

    @POST
    @Path("/insert-invoicevendor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 1, httpMethod = "POST", value = "application/json",
            notes = "Create Invoice Vendor",
            authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response insertInvoiceVendor(ParamInvoice param) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! ",
                        null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ForcaInvoiceImpl forcaInvoiceImpl = new ForcaInvoiceImpl();
                result = forcaInvoiceImpl.insertInvoiceVendor(param,token);
                
            }
        }
        
        return Response.status(Status.OK).entity(result).build();
    }

}

