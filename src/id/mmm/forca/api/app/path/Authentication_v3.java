package id.mmm.forca.api.app.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.compiere.util.Util;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.Authorization;
import id.mmm.forca.api.filter.RestFilter;
import id.mmm.forca.api.impl.AuthLoginImpl;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.auth.AuthenticationApi;
import id.sisi.forca.authenticationapi.response.ResponseData;

@Component
@Path("/authentication/v3")
@Api(position = 1, value = "Authentication", description = "Login Validation to get Forca-Token")
public class Authentication_v3 {
    WebserviceUtil util = new WebserviceUtil();
    AuthenticationApi auth;
    ResponseData respon = new ResponseData();
    ParamRequest param = new ParamRequest();
    AuthenticationApi authAPI = new AuthenticationApi();

    // User Login Idempiere
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 1, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Login")
    public Response getUserForca(
            @ApiParam(value = "Username", required = true) @FormParam("username") String username,
            @ApiParam(value = "Password", required = true) @FormParam("password") String password) {
        ResponseData result = new ResponseData();
        if (Util.isEmpty(username)) {
            result = util.resultResponse("E", "Please input parameter 'username' ", null);
        } else if (Util.isEmpty(password)) {
            result = util.resultResponse("E", "Please input parameter 'password' ", null);
        } else {
            AuthLoginImpl signIn = new AuthLoginImpl();
            result = signIn.getLoginForca(username, password);
        }
        return Response.status(Status.OK).entity(result).build();
    }
    
    @POST
    @Path("/get-userclientforca")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "User Forca", authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getUserClientForca() {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                AuthLoginImpl signIn = new AuthLoginImpl();
                result = signIn.getListUserClientForca();
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }

    @POST
    @Path("/getclients")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    @ApiOperation(position = 3, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Login Forca For Take a Client ID",
            authorizations = {@Authorization(value = "Forca-Token")})
    // @ApiImplicitParams({
    // @ApiImplicitParam(name = "Forca-Token", value = "Token Forca", required = true, dataType =
    // "string", paramType = "header"),
    // @ApiImplicitParam(name = "Forca-Key", value = "Key Forca", required = true, dataType =
    // "string", paramType = "header")
    // })
    public Response getClients(
            @ApiParam(value = "username Forca ERP",
                    required = true) @FormParam("username") String username,
            @ApiParam(value = "password user Forca ERP",
                    required = true) @FormParam("password") String password) {

        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.getClientForca(username, password);
        return Response.status(Status.OK).entity(respon).build();
    }

    @POST
    @Path("/get-lastloginuser")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "User Forca", authorizations = {@Authorization(value = "Forca-Token")})
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getLastLoginUser(
            @ApiParam(value = "periode", required = true) @FormParam("periode") String periode) {
        ResponseData result = new ResponseData();
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            result = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                result = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                param.setToken(token);
                param.setPeriode(periode);
                AuthLoginImpl signIn = new AuthLoginImpl();
                result = signIn.getLastLogin(param);
            }
        }
        return Response.status(Status.OK).entity(result).build();
    }
    
    @POST
    @Path("/getroles")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    @ApiOperation(position = 4, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Role User Forca")
    public Response getRoles(@ApiParam(required = true) @FormParam("username") String username, // @FormParam("password")
                                                                                                // String
                                                                                                // password,
            @ApiParam(required = true) @FormParam("ad_client_id") Integer client_id) {
        param.setUsername(username);
        // param.setPassword(password);
        param.setAd_client_id(client_id);

        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.getRoleForca(param);
        return Response.status(Status.OK).entity(respon).build();
    }

    @POST
    @Path("/getorgs")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    @ApiOperation(position = 5, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Organization User Forca")
    public Response getOrgs(@ApiParam(required = true) @FormParam("username") String username,
            @ApiParam(required = true) @FormParam("ad_client_id") Integer client_id,
            @ApiParam(required = true) @FormParam("ad_role_id") Integer role_id) {
        param.setUsername(username);
        // param.setPassword(password);
        param.setAd_client_id(client_id);
        param.setAd_role_id(role_id);

        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.getOrgForca(param);
        return Response.status(Status.OK).entity(respon).build();
    }

    @POST
    @Path("/getwarehouses")
    @Consumes("application/x-www-form-urlencoded")
    @Produces("application/json")
    @ApiOperation(position = 6, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Warehouse User Forca")
    public Response getWarehouses(@ApiParam(required = true) @FormParam("username") String username,
            @ApiParam(required = true) @FormParam("ad_client_id") Integer client_id,
            @ApiParam(required = true) @FormParam("ad_org_id") Integer org_id) {
        param.setUsername(username);
        param.setAd_client_id(client_id);
        param.setAd_org_id(org_id);

        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.getWarehouseForca(param);
        return Response.status(Status.OK).entity(respon).build();

    }

    @POST
    @Path("/gettoken")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 7, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Token for Request API Forca")
    public Response getToken(@ApiParam(required = true) @FormParam("username") String username,
            @ApiParam(required = true) @FormParam("password") String password,
            @ApiParam(required = true) @FormParam("ad_client_id") Integer client_id,
            @ApiParam(required = true) @FormParam("ad_role_id") Integer role_id,
            @ApiParam(required = true) @FormParam("ad_org_id") Integer org_id,
            @ApiParam(required = true) @FormParam("m_warehouse_id") Integer warehouse_id,
            @ApiParam(required = false,
                    value = "(*) Only POS Apps/Customer") @FormParam("c_bpartner_id") Integer bpartner_id) {

        param.setUsername(username);
        param.setPassword(password);
        param.setAd_client_id(client_id);
        param.setAd_role_id(role_id);
        param.setAd_org_id(org_id);
        param.setM_warehouse_id(warehouse_id);
        param.setC_bpartner_id(bpartner_id);

        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.getTokenApi(param);
        return Response.status(Status.OK).entity(respon).build();
    }

    // refreshtoken
    @POST
    @Path("/refreshtoken")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 8, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Refresh Token When Forca-Token Has Expired")
    public Response refreshToken(@ApiParam(required = true) @FormParam("token") String token) {
        param.setToken(token);
        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.refreshToken(param);
        return Response.status(Status.OK).entity(respon).build();
    }

    // info token
    @POST
    @Path("/infotoken")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(position = 9, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Info Token")
    public Response infoToken(@ApiParam(required = true) @FormParam("token") String token) {

        param.setToken(token);
        AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
        respon = apiLoginImpl.infoToken(param);
        return Response.status(Status.OK).entity(respon).build();
    }

    // resetpassword


}
