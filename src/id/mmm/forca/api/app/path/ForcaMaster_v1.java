package id.mmm.forca.api.app.path;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.compiere.util.Util;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import id.mmm.forca.api.filter.RestFilter;
import id.mmm.forca.api.impl.ForcaCrmCormaImpl;
import id.mmm.forca.api.impl.ForcaMasterImpl;
import id.mmm.forca.api.request.ParamRequest;
import id.mmm.forca.api.request.ParamRequest2;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.auth.AuthenticationApi;
import id.sisi.forca.authenticationapi.response.ResponseData;

@Component
@Path("/master/v1")
@Api(position = 3, value = "Master Data", description = "get Master Data Forca")
public class ForcaMaster_v1 {
    ResponseData respon = new ResponseData();
    ParamRequest2 param = new ParamRequest2();
    WebserviceUtil util = new WebserviceUtil();
    AuthenticationApi authAPI = new AuthenticationApi();
    @POST
    @Path("/get-bpartner")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 0, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Bussiness Partner Info",
            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getBPartner(@ApiParam(value = "Bussiness Partner ID",
            required = true) @FormParam("c_bpartner_id") Integer bpartner_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                this.param.setC_bpartner_id(bpartner_id);
                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetBPartnerByID(this.param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-bpartner-group")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 0, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Get Bussiness Partner Group",
            authorizations = {@com.wordnik.swagger.annotations.Authorization("Forca-Token")})
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getBPGroup(@ApiParam(value = "Bussiness Partner Group ID",
            required = false) @FormParam("c_bp_group_id") int c_bp_group_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                this.param.setC_bp_group_id(Integer.valueOf(c_bp_group_id));
                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetBPGroup(this.param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-warehouse")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 1, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Warehouse")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getWarehouse(
            @ApiParam(required = false) @FormParam("m_warehouse_id") int m_warehouse_id,
            @ApiParam(required = false) @FormParam("name_search") String name_search) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setM_warehouse_id(Integer.valueOf(m_warehouse_id));
                param.setName_search(name_search);

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetWarehouse(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-uom")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Unit Of Measure")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getUOM(@ApiParam(required = false) @FormParam("c_uom_id") int c_uom_id,
            @ApiParam(required = false) @FormParam("name_search") String name_search) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setC_uom_id(Integer.valueOf(c_uom_id));
                param.setName_search(name_search);

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetUOM(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-product")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Product ERP")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getProduct(
            @ApiParam(required = false) @FormParam("m_product_id") int m_product_id,
            @ApiParam(required = false) @FormParam("name_search") String name_search,
            @ApiParam(value = "Y/N",
                    required = false) @FormParam("isproductcrm") String isproductcrm,
            @ApiParam(value = "Y/N", required = false) @FormParam("issold") String issold) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setM_product_id(Integer.valueOf(m_product_id));
                param.setName_search(name_search);
                param.setIsproductcrm(isproductcrm);
                param.setIssold(issold);

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetProduct(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-product-category")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Product-Category")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getProductCategory(@ApiParam(
            required = false) @FormParam("m_product_category_id") int m_product_category_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setM_product_category_id(Integer.valueOf(m_product_category_id));

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetProductCategory(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-tax-category")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Tax-Category")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getTaxCategory(
            @ApiParam(required = false) @FormParam("c_taxcategory_id") int c_taxcategory_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setC_taxcategory_id(Integer.valueOf(c_taxcategory_id));

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetTaxCategory(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-org")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Organization")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getOrg(@ApiParam(required = false) @FormParam("ad_org_id") int ad_org_id) {
        ParamRequest2 param = new ParamRequest2();
        param.setAd_org_id(Integer.valueOf(ad_org_id));

        ForcaMasterImpl master = new ForcaMasterImpl();
        this.respon = master.actGetOrg(param);
        AuthenticationApi.countErrorSukses(this.respon);
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-orgtrx")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Transaction Organization")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getOrgTrx(
            @ApiParam(required = false) @FormParam("ad_orgtrx_id") int ad_org_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setAd_org_id(Integer.valueOf(ad_org_id));

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetOrgTrx(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-project")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(httpMethod = "POST", value = "form-data", notes = "Select Project")
    @ApiImplicitParams({@ApiImplicitParam(name = "Forca-Token", value = "Token Forca",
            required = true, dataType = "string", paramType = "header")})
    public Response getProject(
            @ApiParam(required = false) @FormParam("c_project_id") int c_project_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setC_project_id(c_project_id);

                ForcaMasterImpl master = new ForcaMasterImpl();
                this.respon = master.actGetProject(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }
}
