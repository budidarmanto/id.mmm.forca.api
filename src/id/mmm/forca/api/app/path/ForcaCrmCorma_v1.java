package id.mmm.forca.api.app.path;


import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import org.compiere.util.Util;
import org.springframework.stereotype.Component;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import id.mmm.forca.api.filter.RestFilter;
import id.mmm.forca.api.impl.AuthLoginImpl;
import id.mmm.forca.api.impl.ForcaCrmCormaImpl;
import id.mmm.forca.api.request.ParamRequest2;
import id.mmm.forca.api.util.Constants;
import id.mmm.forca.api.util.WebserviceUtil;
import id.sisi.forca.authenticationapi.auth.AuthenticationApi;
import id.sisi.forca.authenticationapi.response.ResponseData;

@Component
@Path("/crm-corma/v1")
@Api(position = 5, value = "Crm-Corma", description = "CRM Integration Forca")
public class ForcaCrmCorma_v1 {
    ResponseData respon = new ResponseData();
    ParamRequest2 param = new ParamRequest2();
    WebserviceUtil util = new WebserviceUtil();
    AuthenticationApi authAPI = new AuthenticationApi();

    @POST
    @Path("/set-partner")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 0, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Upsert Business Partner ")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setPartner(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(value = "diisi jika update",
                    required = false) @FormParam("c_bpartner_id") int c_bpartner_id,
            @ApiParam(value = "diisi jika update",
                    required = false) @FormParam("c_location_id") int c_location_id,
            @ApiParam(required = true) @FormParam("name_partner") String name_partner,
            @ApiParam(required = false) @FormParam("referenceno") String referenceno,
            @ApiParam(required = true) @FormParam("address1") String address1,
            @ApiParam(required = false) @FormParam("address2") String address2,
            @ApiParam(required = false) @FormParam("address3") String address3,
            @ApiParam(required = false) @FormParam("address4") String address4,
            @ApiParam(required = false) @FormParam("c_country_id") int c_country_id,
            @ApiParam(required = false) @FormParam("c_region_id") int c_region_id,
            @ApiParam(required = false) @FormParam("c_city_id") int c_city_id) {

        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setC_bpartner_id(Integer.valueOf(c_bpartner_id));

                param.setC_location_id(Integer.valueOf(c_location_id));
                param.setReferenceno(referenceno);

                param.setName_partner(name_partner);

                param.setAddress1(address1);
                param.setAddress2(address2);
                param.setAddress3(address3);
                param.setAddress4(address4);
                if (c_country_id > 0) {
                    param.setC_country_id(Integer.valueOf(c_country_id));
                } else {
                    param.setC_country_id(Integer.valueOf(209));
                }
                param.setC_region_id(Integer.valueOf(c_region_id));
                param.setC_city_id(Integer.valueOf(c_city_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetPartner(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-user-contact")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 1, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Insert Contact Bpartner ")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setUserContact(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("c_bpartner_id") int c_bpartner_id,
            @ApiParam(required = false) @FormParam("ad_user_id") int ad_user_id,
            @ApiParam(required = true) @FormParam("name") String name,
            @ApiParam(required = false) @FormParam("description") String description,
            @ApiParam(required = false) @FormParam("email_contact") String email_contact,
            @ApiParam(required = false) @FormParam("phone_contact") String phone_contact,
            @ApiParam(required = false) @FormParam("phone2_contact") String phone2_contact,
            @ApiParam(required = false) @FormParam("fax") String fax,
            @ApiParam(required = false) @FormParam("title") String title) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setC_bpartner_id(Integer.valueOf(c_bpartner_id));
                param.setAd_user_id(Integer.valueOf(ad_user_id));
                param.setName(name);
                param.setDescription(description);
                param.setEmail_contact(email_contact);
                param.setPhone_contact(phone_contact);
                param.setPhone2_contact(phone2_contact);
                param.setFax(fax);
                param.setTitle(title);

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetUserContact(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-channel")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 2, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Insert Channel")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setChannel(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(value = "diisi jika update",
                    required = false) @FormParam("c_channel_id") int c_channel_id,
            @ApiParam(required = true) @FormParam("name") String name,
            @ApiParam(required = true) @FormParam("description") String description) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setC_channel_id(Integer.valueOf(c_channel_id));
                param.setName(name);
                param.setDescription(description);

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetChannel(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-campaign")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 3, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Insert Campaign")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setCampaign(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(value = "diisi jika update",
                    required = false) @FormParam("c_campaign_id") int c_campaign_id,
            @ApiParam(required = true) @FormParam("c_channel_id") int c_channel_id,
            @ApiParam(required = true) @FormParam("name") String name,
            @ApiParam(required = true) @FormParam("description") String description,
            @ApiParam(value = "format: yyyy-mm-dd",
                    required = true) @FormParam("startdate") String startdate,
            @ApiParam(value = "format: yyyy-mm-dd",
                    required = true) @FormParam("enddate") String enddate,
            @ApiParam(required = true) @FormParam("cost") Double cost) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setC_campaign_id(Integer.valueOf(c_campaign_id));
                param.setC_channel_id(Integer.valueOf(c_channel_id));
                param.setName(name);
                param.setDescription(description);
                param.setDatefrom(startdate);
                param.setDateto(enddate);
                param.setCost(cost.doubleValue());

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();

                this.respon = impl.actSetCampaign(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-sales-order")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 4, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Insert Sales Order")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setSalesOrder(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("ad_org_id") int ad_org_id,
            @ApiParam(required = true) @FormParam("ad_orgtrx_id") int ad_orgtrx_id,
            @ApiParam(required = false) @FormParam("c_order_id") int c_order_id,
            @ApiParam(required = true) @FormParam("c_bpartner_id") int c_bpartner_id,
            @ApiParam(
                    required = true) @FormParam("c_bpartner_location_id") int c_bpartner_location_id,
            @ApiParam(required = false) @FormParam("m_warehouse_id") int m_warehouse_id,
            @ApiParam(required = false) @FormParam("c_opportunity_id") int c_opportunity_id,
            @ApiParam(required = false) @FormParam("c_campaign_id") int c_campaign_id,
            @ApiParam(required = true) @FormParam("dateordered") String dateordered,
            @ApiParam(required = false) @FormParam("documentno") String documentno,
            @ApiParam(required = false) @FormParam("ad_user_id") int ad_user_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setAd_org_id(Integer.valueOf(ad_org_id));
                param.setAd_orgtrx_id(Integer.valueOf(ad_orgtrx_id));
                param.setC_order_id(Integer.valueOf(c_order_id));
                param.setC_bpartner_id(Integer.valueOf(c_bpartner_id));
                param.setC_bpartner_location_id(Integer.valueOf(c_bpartner_location_id));
                param.setM_warehouse_id(Integer.valueOf(m_warehouse_id));
                param.setC_opportunity_id(Integer.valueOf(c_opportunity_id));
                param.setC_campaign_id(Integer.valueOf(c_campaign_id));
                param.setDatefrom(dateordered);
                param.setDocumentno(documentno);
                param.setAd_user_id(Integer.valueOf(ad_user_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();

                this.respon = impl.actSetSalesOrder(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-sales-order-line")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(position = 5, httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Insert Sales Order Line")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setSalesOrderLine(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id,
            @ApiParam(required = true) @FormParam("ad_orgtrx_id") int ad_orgtrx_id,
            @ApiParam(required = false) @FormParam("c_orderline_id") int c_orderline_id,
            @ApiParam(required = true) @FormParam("qty") Double qty,
            @ApiParam(required = false) @FormParam("priceactual") Double priceactual,
            @ApiParam(required = true) @FormParam("m_product_id") int m_product_id,
            @ApiParam(required = true) @FormParam("c_uom_id") int c_uom_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));

                param.setC_order_id(Integer.valueOf(c_order_id));
                param.setAd_orgtrx_id(Integer.valueOf(ad_orgtrx_id));

                param.setM_product_id(Integer.valueOf(m_product_id));
                param.setC_orderline_id(Integer.valueOf(c_orderline_id));
                param.setQty(qty.doubleValue());
                param.setPriceactual(priceactual.doubleValue());
                param.setC_uom_id(Integer.valueOf(c_uom_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();

                this.respon = impl.actSetSalesOrderLine(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-crm-product")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Crm ID to Product")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setCrmProduct(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("m_product_id") int m_product_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setM_product_id(Integer.valueOf(m_product_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetCrmProduct(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-crm-org")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Crm ID to Organization")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setCrmOrg(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("ad_org_id") int ad_org_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setAd_org_id(Integer.valueOf(ad_org_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetCrmOrg(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-crm-product-category")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Crm ID to Product Category")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setCrmProductCategory(
            @ApiParam(required = true) @FormParam("crm_id") int crm_id, @ApiParam(
                    required = true) @FormParam("m_product_category_id") int m_product_category_id) {
        ParamRequest2 param = new ParamRequest2();
        param.setCrm_id(Integer.valueOf(crm_id));
        param.setM_product_category_id(Integer.valueOf(m_product_category_id));

        ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
        this.respon = impl.actSetCrmProductCategory(param);
        AuthenticationApi.countErrorSukses(this.respon);
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-crm-uom")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Crm ID to UOM")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setCrmUom(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("c_uom_id") int c_uom_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setC_uom_id(Integer.valueOf(c_uom_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetCrmUom(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/set-crm-tax-category")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Update Crm ID to Tax Category")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response setCrmTaxCategory(@ApiParam(required = true) @FormParam("crm_id") int crm_id,
            @ApiParam(required = true) @FormParam("c_taxcategory_id") int c_taxcategory_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setCrm_id(Integer.valueOf(crm_id));
                param.setC_taxcategory_id(Integer.valueOf(c_taxcategory_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetCrmTaxCategory(param);
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/get-status-order")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Select Product-Category")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getStatusOrder(
            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setC_order_id(Integer.valueOf(c_order_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetStatusOrder(param, "VIEW");
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/complete-order")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Action Complete Order")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getCompleteOrder(
            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setC_order_id(Integer.valueOf(c_order_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetStatusOrder(param, "CO");
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }

    @POST
    @Path("/void-order")
    @Consumes({"application/x-www-form-urlencoded"})
    @Produces({"application/json"})
    @ApiOperation(httpMethod = "POST", value = "x-www-form-urlencoded",
            notes = "Action Complete Order")
    @ApiImplicitParams({@com.wordnik.swagger.annotations.ApiImplicitParam(name = "Forca-Token",
            value = "Token Forca", required = true, dataType = "string", paramType = "header")})
    public Response getVoidOrder(
            @ApiParam(required = true) @FormParam("c_order_id") int c_order_id) {
        HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
        String token = httpServletRequest.getHeader(Constants.AUTH_FORCA_TOKEN);
        if (Util.isEmpty(token)) {
            respon = util.resultResponse("E", "Please input header " + Constants.AUTH_FORCA_TOKEN,
                    null);
        } else {
            if (!authAPI.validasiToken(token)) {
                respon = util.resultResponse("E", "Wrong Token! ", null);
            } else {
                AuthenticationApi.setContextByToken(token);
                ParamRequest2 param = new ParamRequest2();
                param.setC_order_id(Integer.valueOf(c_order_id));

                ForcaCrmCormaImpl impl = new ForcaCrmCormaImpl();
                this.respon = impl.actSetStatusOrder(param, "VO");
                AuthenticationApi.countErrorSukses(this.respon);
            }
        }
        
        return Response.status(Response.Status.OK).entity(this.respon).build();
    }
}
