package id.mmm.forca.api.request;

import java.util.List;

public class ParamOrder {
    String document_no;
    String m_warehouse_id;
    String m_pricelist_id;
    String salerep_id;
    String payment_rule;
    String ad_org_id;
    List<ParamOrderLine> list_line;
    String c_bpartner_id;
    
    public String getC_bpartner_id() {
        return c_bpartner_id;
    }
    public void setC_bpartner_id(String c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }
    public String getAd_org_id() {
        return ad_org_id;
    }
    public void setAd_org_id(String ad_org_id) {
        this.ad_org_id = ad_org_id;
    }
    public String getDocument_no() {
        return document_no;
    }
    public void setDocument_no(String document_no) {
        this.document_no = document_no;
    }
    public String getM_warehouse_id() {
        return m_warehouse_id;
    }
    public void setM_warehouse_id(String m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }
    public String getM_pricelist_id() {
        return m_pricelist_id;
    }
    public void setM_pricelist_id(String m_pricelist_id) {
        this.m_pricelist_id = m_pricelist_id;
    }
    public String getSalerep_id() {
        return salerep_id;
    }
    public void setSalerep_id(String salerep_id) {
        this.salerep_id = salerep_id;
    }
    public String getPayment_rule() {
        return payment_rule;
    }
    public void setPayment_rule(String payment_rule) {
        this.payment_rule = payment_rule;
    }
    public List<ParamOrderLine> getList_line() {
        return list_line;
    }
    public void setList_line(List<ParamOrderLine> list_line) {
        this.list_line = list_line;
    }
    
}
