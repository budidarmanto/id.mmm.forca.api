package id.mmm.forca.api.request;

import java.sql.Timestamp;

public class ParamRequest2 {
    String username, password, xtglfrom, xtglto, xtglorderfrom, xtglorderto,token;

    Integer ad_client_id, ad_user_id, ad_role_id, ad_org_id;
    Integer m_warehouse_id, m_product_id, m_production_id, m_inout_id;

    Integer c_order_id, c_orderline_id, c_bpartner_id, c_project_id;
    Integer c_invoice_id, c_payment_id, r_sppd_id;

    String ad_wf_name, product_code, ad_wf_activity_id, message, yesno, ad_wf_node_id,
            name_search;
    
    String isonetime, issalesrep, isvendor, iscustomer, isemployee, issold,
            ispurchased;
    String documentno, issotrx, isreceipt,pos_order_id,retail_id,dateorder;


    String enrollnumber, verifymode, inoutmode, datelog, workcode, ip_address,
            attstate;
    Double qty,priceactual; 
     
    String datefrom, dateto, referenceno;
    Integer ad_orgtrx_id, crm_id;
    Integer c_channel_id, c_location_id, c_campaign_id, c_opportunity_id,
            c_bpartner_location_id, c_uom_id, m_product_category_id;
    Integer c_taxcategory_id, c_bp_status_id, c_country_id, c_region_id,
            c_city_id, c_bp_group_id;
    double salesvolume, cost;
    String isactive, isproductcrm;
    String name, description, producttype, name_partner, value_partner,
            name_contact, value_contact, address1, address2, address3, address4;
    String email_contact, phone_contact, phone2_contact, birthday, fax, title;
    
    public Double getPriceactual() {
        return priceactual;
    }

    public void setPriceactual(Double priceactual) {
        this.priceactual = priceactual;
    }

    public String getDatefrom() {
        return datefrom;
    }

    public void setDatefrom(String datefrom) {
        this.datefrom = datefrom;
    }

    public String getDateto() {
        return dateto;
    }

    public void setDateto(String dateto) {
        this.dateto = dateto;
    }

    public String getReferenceno() {
        return referenceno;
    }

    public void setReferenceno(String referenceno) {
        this.referenceno = referenceno;
    }

    public Integer getAd_orgtrx_id() {
        return ad_orgtrx_id;
    }

    public void setAd_orgtrx_id(Integer ad_orgtrx_id) {
        this.ad_orgtrx_id = ad_orgtrx_id;
    }

    public Integer getCrm_id() {
        return crm_id;
    }

    public void setCrm_id(Integer crm_id) {
        this.crm_id = crm_id;
    }

    public Integer getC_channel_id() {
        return c_channel_id;
    }

    public void setC_channel_id(Integer c_channel_id) {
        this.c_channel_id = c_channel_id;
    }

    public Integer getC_location_id() {
        return c_location_id;
    }

    public void setC_location_id(Integer c_location_id) {
        this.c_location_id = c_location_id;
    }

    public Integer getC_campaign_id() {
        return c_campaign_id;
    }

    public void setC_campaign_id(Integer c_campaign_id) {
        this.c_campaign_id = c_campaign_id;
    }

    public Integer getC_opportunity_id() {
        return c_opportunity_id;
    }

    public void setC_opportunity_id(Integer c_opportunity_id) {
        this.c_opportunity_id = c_opportunity_id;
    }

    public Integer getC_bpartner_location_id() {
        return c_bpartner_location_id;
    }

    public void setC_bpartner_location_id(Integer c_bpartner_location_id) {
        this.c_bpartner_location_id = c_bpartner_location_id;
    }

    public Integer getC_uom_id() {
        return c_uom_id;
    }

    public void setC_uom_id(Integer c_uom_id) {
        this.c_uom_id = c_uom_id;
    }

    public Integer getM_product_category_id() {
        return m_product_category_id;
    }

    public void setM_product_category_id(Integer m_product_category_id) {
        this.m_product_category_id = m_product_category_id;
    }

    public Integer getC_taxcategory_id() {
        return c_taxcategory_id;
    }

    public void setC_taxcategory_id(Integer c_taxcategory_id) {
        this.c_taxcategory_id = c_taxcategory_id;
    }

    public Integer getC_bp_status_id() {
        return c_bp_status_id;
    }

    public void setC_bp_status_id(Integer c_bp_status_id) {
        this.c_bp_status_id = c_bp_status_id;
    }

    public Integer getC_country_id() {
        return c_country_id;
    }

    public void setC_country_id(Integer c_country_id) {
        this.c_country_id = c_country_id;
    }

    public Integer getC_region_id() {
        return c_region_id;
    }

    public void setC_region_id(Integer c_region_id) {
        this.c_region_id = c_region_id;
    }

    public Integer getC_city_id() {
        return c_city_id;
    }

    public void setC_city_id(Integer c_city_id) {
        this.c_city_id = c_city_id;
    }

    public Integer getC_bp_group_id() {
        return c_bp_group_id;
    }

    public void setC_bp_group_id(Integer c_bp_group_id) {
        this.c_bp_group_id = c_bp_group_id;
    }

    public double getSalesvolume() {
        return salesvolume;
    }

    public void setSalesvolume(double salesvolume) {
        this.salesvolume = salesvolume;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public String getIsproductcrm() {
        return isproductcrm;
    }

    public void setIsproductcrm(String isproductcrm) {
        this.isproductcrm = isproductcrm;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    public String getName_partner() {
        return name_partner;
    }

    public void setName_partner(String name_partner) {
        this.name_partner = name_partner;
    }

    public String getValue_partner() {
        return value_partner;
    }

    public void setValue_partner(String value_partner) {
        this.value_partner = value_partner;
    }

    public String getName_contact() {
        return name_contact;
    }

    public void setName_contact(String name_contact) {
        this.name_contact = name_contact;
    }

    public String getValue_contact() {
        return value_contact;
    }

    public void setValue_contact(String value_contact) {
        this.value_contact = value_contact;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getEmail_contact() {
        return email_contact;
    }

    public void setEmail_contact(String email_contact) {
        this.email_contact = email_contact;
    }

    public String getPhone_contact() {
        return phone_contact;
    }

    public void setPhone_contact(String phone_contact) {
        this.phone_contact = phone_contact;
    }

    public String getPhone2_contact() {
        return phone2_contact;
    }

    public void setPhone2_contact(String phone2_contact) {
        this.phone2_contact = phone2_contact;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateorder() {
        return dateorder;
    }

    public void setDateorder(String dateorder) {
        this.dateorder = dateorder;
    }

    public String getRetail_id() {
        return retail_id;
    }

    public void setRetail_id(String retail_id) {
        this.retail_id = retail_id;
    }

    public String getPos_order_id() {
        return pos_order_id;
    }

    public void setPos_order_id(String pos_order_id) {
        this.pos_order_id = pos_order_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }
    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }
    public String getEnrollnumber() {
        return enrollnumber;
    }

    public void setEnrollnumber(String enrollnumber) {
        this.enrollnumber = enrollnumber;
    }

    public String getVerifymode() {
        return verifymode;
    }

    public void setVerifymode(String verifymode) {
        this.verifymode = verifymode;
    }

    public String getInoutmode() {
        return inoutmode;
    }

    public void setInoutmode(String inoutmode) {
        this.inoutmode = inoutmode;
    }

    public String getDatelog() {
        return datelog;
    }

    public void setDatelog(String datelog) {
        this.datelog = datelog;
    }

    public String getWorkcode() {
        return workcode;
    }

    public void setWorkcode(String workcode) {
        this.workcode = workcode;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public void setAttstate(String attstate) {
        this.attstate = attstate;
    }

    public String getAttstate() {
        return attstate;
    }

    public Integer getM_inout_id() {
        return m_inout_id;
    }

    public void setM_inout_id(Integer m_inout_id) {
        this.m_inout_id = m_inout_id;
    }

    public Integer getM_production_id() {
        return m_production_id;
    }

    public void setM_production_id(Integer m_production_id) {
        this.m_production_id = m_production_id;
    }

    public String getIsreceipt() {
        return isreceipt;
    }

    public void setIsreceipt(String isreceipt) {
        this.isreceipt = isreceipt;
    }

    public String getIssotrx() {
        return issotrx;
    }

    public void setIssotrx(String issotrx) {
        this.issotrx = issotrx;
    }

    public String getXtglorderfrom() {
        return xtglorderfrom;
    }

    public void setXtglorderfrom(String xtglorderfrom) {
        this.xtglorderfrom = xtglorderfrom;
    }

    public String getXtglorderto() {
        return xtglorderto;
    }

    public void setXtglorderto(String xtglorderto) {
        this.xtglorderto = xtglorderto;
    }

    public Integer getR_sppd_id() {
        return r_sppd_id;
    }

    public void setR_sppd_id(Integer r_sppd_id) {
        this.r_sppd_id = r_sppd_id;
    }

    public String getDocumentno() {
        return documentno;
    }

    public void setDocumentno(String documentno) {
        this.documentno = documentno;
    }

    public Integer getC_invoice_id() {
        return c_invoice_id;
    }

    public void setC_invoice_id(Integer c_invoice_id) {
        this.c_invoice_id = c_invoice_id;
    }

    public Integer getC_payment_id() {
        return c_payment_id;
    }

    public void setC_payment_id(Integer c_payment_id) {
        this.c_payment_id = c_payment_id;
    }

    public String getIssold() {
        return issold;
    }

    public void setIssold(String issold) {
        this.issold = issold;
    }

    public String getIspurchased() {
        return ispurchased;
    }

    public void setIspurchased(String ispurchased) {
        this.ispurchased = ispurchased;
    }

    public Integer getM_product_id() {
        return m_product_id;
    }

    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }

    public Integer getM_warehouse_id() {
        return m_warehouse_id;
    }

    public void setM_warehouse_id(Integer m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }

    public String getName_search() {
        return name_search;
    }

    public void setName_search(String name_search) {
        this.name_search = name_search;
    }

    public Integer getC_project_id() {
        return c_project_id;
    }

    public void setC_project_id(Integer c_project_id) {
        this.c_project_id = c_project_id;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public String getIsonetime() {
        return isonetime;
    }

    public void setIsonetime(String isonetime) {
        this.isonetime = isonetime;
    }

    public String getIssalesrep() {
        return issalesrep;
    }

    public void setIssalesrep(String issalesrep) {
        this.issalesrep = issalesrep;
    }

    public String getIsvendor() {
        return isvendor;
    }

    public void setIsvendor(String isvendor) {
        this.isvendor = isvendor;
    }

    public String getIscustomer() {
        return iscustomer;
    }

    public void setIscustomer(String iscustomer) {
        this.iscustomer = iscustomer;
    }

    public String getIsemployee() {
        return isemployee;
    }

    public void setIsemployee(String isemployee) {
        this.isemployee = isemployee;
    }

    public String getAd_wf_node_id() {
        return ad_wf_node_id;
    }

    public void setAd_wf_node_id(String ad_wf_node_id) {
        this.ad_wf_node_id = ad_wf_node_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getYesno() {
        return yesno;
    }

    public void setYesno(String yesno) {
        this.yesno = yesno;
    }

    public String getAd_wf_name() {
        return ad_wf_name;
    }

    public void setAd_wf_name(String ad_wf_name) {
        this.ad_wf_name = ad_wf_name;
    }

    public String getAd_wf_activity_id() {
        return ad_wf_activity_id;
    }

    public void setAd_wf_activity_id(String ad_wf_activity_id) {
        this.ad_wf_activity_id = ad_wf_activity_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAd_client_id() {
        return ad_client_id;
    }

    public void setAd_client_id(Integer ad_client_id) {
        this.ad_client_id = ad_client_id;
    }

    public Integer getAd_user_id() {
        return ad_user_id;
    }

    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }

    public Integer getAd_role_id() {
        return ad_role_id;
    }

    public void setAd_role_id(Integer ad_role_id) {
        this.ad_role_id = ad_role_id;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public Integer getC_orderline_id() {
        return c_orderline_id;
    }

    public void setC_orderline_id(Integer c_orderline_id) {
        this.c_orderline_id = c_orderline_id;
    }

    public String getXtglfrom() {
        return xtglfrom;
    }

    public void setXtglfrom(String xtglfrom) {
        this.xtglfrom = xtglfrom;
    }

    public String getXtglto() {
        return xtglto;
    }

    public void setXtglto(String xtglto) {
        this.xtglto = xtglto;
    }

}
