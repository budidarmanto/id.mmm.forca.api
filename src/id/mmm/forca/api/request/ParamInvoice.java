package id.mmm.forca.api.request;

import java.util.List;

public class ParamInvoice {

    String description_header;
    String c_bpartner_id;
    String ad_org_id;
    List<ParamInvoiceLine> line_list;
    public String getDescription_header() {
        return description_header;
    }
    public void setDescription_header(String description_header) {
        this.description_header = description_header;
    }
    public String getC_bpartner_id() {
        return c_bpartner_id;
    }
    public void setC_bpartner_id(String c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }
    public String getAd_org_id() {
        return ad_org_id;
    }
    public void setAd_org_id(String ad_org_id) {
        this.ad_org_id = ad_org_id;
    }
    public List<ParamInvoiceLine> getLine_list() {
        return line_list;
    }
    public void setLine_list(List<ParamInvoiceLine> line_list) {
        this.line_list = line_list;
    }
  
    
}
