package id.mmm.forca.api.request;

public class ParamInvoiceLine {

    String c_charge_id;
    String price_entered;
    
    public String getC_charge_id() {
        return c_charge_id;
    }
    public void setC_charge_id(String c_charge_id) {
        this.c_charge_id = c_charge_id;
    }
    public String getPrice_entered() {
        return price_entered;
    }
    public void setPrice_entered(String price_entered) {
        this.price_entered = price_entered;
    }
}
