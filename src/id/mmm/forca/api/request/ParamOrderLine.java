package id.mmm.forca.api.request;

public class ParamOrderLine {
    String m_product_id;
    String qty_entered;
    String price_entered;
    String c_tax_id;
    
    public String getM_product_id() {
        return m_product_id;
    }
    public void setM_product_id(String m_product_id) {
        this.m_product_id = m_product_id;
    }
    public String getQty_entered() {
        return qty_entered;
    }
    public void setQty_entered(String qty_entered) {
        this.qty_entered = qty_entered;
    }
    public String getPrice_entered() {
        return price_entered;
    }
    public void setPrice_entered(String price_entered) {
        this.price_entered = price_entered;
    }
    public String getC_tax_id() {
        return c_tax_id;
    }
    public void setC_tax_id(String c_tax_id) {
        this.c_tax_id = c_tax_id;
    }
    
}
