package id.mmm.forca.api.request;

import java.util.List;
import java.util.Map;

public class ParamRequest {
    String username, password, x_tgl_from, x_tgl_to, x_tgl_order_from, x_tgl_order_to, token;

    Integer ad_client_id, ad_user_id, ad_role_id, ad_org_id;
    Integer m_warehouse_id, m_product_id, m_production_id, m_inout_id;

    Integer c_order_id, c_orderline_id, c_bpartner_id, c_project_id;
    Integer c_invoice_id, c_payment_id, r_sppd_id;

    String ad_wf_name, product_code, ad_wf_activity_id, message, yesno, ad_wf_node_id, name_search;

    String is_one_time, is_sales_rep, is_vendor, is_customer, is_employee, is_sold, is_purchased;
    String document_no, is_so_trx, is_receipt, pos_order_id, retail_id, date_order;


    String enroll_number, verify_mode, inout_mode, date_log, work_code, ip_address, att_state;

    String platform, version, device_id, model, manufaktur, status;
    Object object_param;
    
    String periode;

    
    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public Object getObject_param() {
        return object_param;
    }

    public void setObject_param(Object object_param) {
        this.object_param = object_param;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufaktur() {
        return manufaktur;
    }

    public void setManufaktur(String manufaktur) {
        this.manufaktur = manufaktur;
    }

    Double qty;

    // tambahan
    String sap_code, id_customer, name_cust, pos_customer_id, so_pos;
    Double credit_limit;

    public String getSO_POS() {
        return so_pos;
    }

    public void setSO_POS(String so_pos) {
        this.so_pos = so_pos;
    }


    public String getPos_customer_id() {
        return pos_customer_id;
    }

    public void setPos_customer_id(String pos_customer_id) {
        this.pos_customer_id = pos_customer_id;
    }

    public String getName_cust() {
        return name_cust;
    }

    public void setName_cust(String name_cust) {
        this.name_cust = name_cust;
    }

    public Double getCredit_limit() {
        return credit_limit;
    }

    public void setCredit_limit(Double credit_limit) {
        this.credit_limit = credit_limit;
    }

    public String getSap_code() {
        return sap_code;
    }

    public void setSap_code(String sap_code) {
        this.sap_code = sap_code;
    }

    public String getId_customer() {
        return id_customer;
    }

    public void setId_customer(String id_customer) {
        this.id_customer = id_customer;
    }

    public String getDateorder() {
        return date_order;
    }

    public void setDateorder(String dateorder) {
        this.date_order = dateorder;
    }

    public String getRetail_id() {
        return retail_id;
    }

    public void setRetail_id(String retail_id) {
        this.retail_id = retail_id;
    }

    public String getPos_order_id() {
        return pos_order_id;
    }

    public void setPos_order_id(String pos_order_id) {
        this.pos_order_id = pos_order_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getEnrollnumber() {
        return enroll_number;
    }

    public void setEnrollnumber(String enrollnumber) {
        this.enroll_number = enrollnumber;
    }

    public String getVerifymode() {
        return verify_mode;
    }

    public void setVerifymode(String verifymode) {
        this.verify_mode = verifymode;
    }

    public String getInoutmode() {
        return inout_mode;
    }

    public void setInoutmode(String inoutmode) {
        this.inout_mode = inoutmode;
    }

    public String getDatelog() {
        return date_log;
    }

    public void setDatelog(String datelog) {
        this.date_log = datelog;
    }

    public String getWorkcode() {
        return work_code;
    }

    public void setWorkcode(String workcode) {
        this.work_code = workcode;
    }

    public String getIp_address() {
        return ip_address;
    }

    public void setIp_address(String ip_address) {
        this.ip_address = ip_address;
    }

    public void setAttstate(String attstate) {
        this.att_state = attstate;
    }

    public String getAttstate() {
        return att_state;
    }

    public Integer getM_inout_id() {
        return m_inout_id;
    }

    public void setM_inout_id(Integer m_inout_id) {
        this.m_inout_id = m_inout_id;
    }

    public Integer getM_production_id() {
        return m_production_id;
    }

    public void setM_production_id(Integer m_production_id) {
        this.m_production_id = m_production_id;
    }

    public String getIsreceipt() {
        return is_receipt;
    }

    public void setIsreceipt(String isreceipt) {
        this.is_receipt = isreceipt;
    }

    public String getIssotrx() {
        return is_so_trx;
    }

    public void setIssotrx(String issotrx) {
        this.is_so_trx = issotrx;
    }

    public String getXtglorderfrom() {
        return x_tgl_order_from;
    }

    public void setXtglorderfrom(String xtglorderfrom) {
        this.x_tgl_order_from = xtglorderfrom;
    }

    public String getXtglorderto() {
        return x_tgl_order_to;
    }

    public void setXtglorderto(String xtglorderto) {
        this.x_tgl_order_to = xtglorderto;
    }

    public Integer getR_sppd_id() {
        return r_sppd_id;
    }

    public void setR_sppd_id(Integer r_sppd_id) {
        this.r_sppd_id = r_sppd_id;
    }

    public String getDocumentno() {
        return document_no;
    }

    public void setDocumentno(String documentno) {
        this.document_no = documentno;
    }

    public Integer getC_invoice_id() {
        return c_invoice_id;
    }

    public void setC_invoice_id(Integer c_invoice_id) {
        this.c_invoice_id = c_invoice_id;
    }

    public Integer getC_payment_id() {
        return c_payment_id;
    }

    public void setC_payment_id(Integer c_payment_id) {
        this.c_payment_id = c_payment_id;
    }

    public String getIssold() {
        return is_sold;
    }

    public void setIssold(String issold) {
        this.is_sold = issold;
    }

    public String getIspurchased() {
        return is_purchased;
    }

    public void setIspurchased(String ispurchased) {
        this.is_purchased = ispurchased;
    }

    public Integer getM_product_id() {
        return m_product_id;
    }

    public void setM_product_id(Integer m_product_id) {
        this.m_product_id = m_product_id;
    }

    public Integer getM_warehouse_id() {
        return m_warehouse_id;
    }

    public void setM_warehouse_id(Integer m_warehouse_id) {
        this.m_warehouse_id = m_warehouse_id;
    }

    public String getName_search() {
        return name_search;
    }

    public void setName_search(String name_search) {
        this.name_search = name_search;
    }

    public Integer getC_project_id() {
        return c_project_id;
    }

    public void setC_project_id(Integer c_project_id) {
        this.c_project_id = c_project_id;
    }

    public Integer getC_bpartner_id() {
        return c_bpartner_id;
    }

    public void setC_bpartner_id(Integer c_bpartner_id) {
        this.c_bpartner_id = c_bpartner_id;
    }

    public String getIsonetime() {
        return is_one_time;
    }

    public void setIsonetime(String isonetime) {
        this.is_one_time = isonetime;
    }

    public String getIssalesrep() {
        return is_sales_rep;
    }

    public void setIssalesrep(String issalesrep) {
        this.is_sales_rep = issalesrep;
    }

    public String getIsvendor() {
        return is_vendor;
    }

    public void setIsvendor(String isvendor) {
        this.is_vendor = isvendor;
    }

    public String getIscustomer() {
        return is_customer;
    }

    public void setIscustomer(String iscustomer) {
        this.is_customer = iscustomer;
    }

    public String getIsemployee() {
        return is_employee;
    }

    public void setIsemployee(String isemployee) {
        this.is_employee = isemployee;
    }

    public String getAd_wf_node_id() {
        return ad_wf_node_id;
    }

    public void setAd_wf_node_id(String ad_wf_node_id) {
        this.ad_wf_node_id = ad_wf_node_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getYesno() {
        return yesno;
    }

    public void setYesno(String yesno) {
        this.yesno = yesno;
    }

    public String getAd_wf_name() {
        return ad_wf_name;
    }

    public void setAd_wf_name(String ad_wf_name) {
        this.ad_wf_name = ad_wf_name;
    }

    public String getAd_wf_activity_id() {
        return ad_wf_activity_id;
    }

    public void setAd_wf_activity_id(String ad_wf_activity_id) {
        this.ad_wf_activity_id = ad_wf_activity_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public Integer getAd_org_id() {
        return ad_org_id;
    }

    public void setAd_org_id(Integer ad_org_id) {
        this.ad_org_id = ad_org_id;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAd_client_id() {
        return ad_client_id;
    }

    public void setAd_client_id(Integer ad_client_id) {
        this.ad_client_id = ad_client_id;
    }

    public Integer getAd_user_id() {
        return ad_user_id;
    }

    public void setAd_user_id(Integer ad_user_id) {
        this.ad_user_id = ad_user_id;
    }

    public Integer getAd_role_id() {
        return ad_role_id;
    }

    public void setAd_role_id(Integer ad_role_id) {
        this.ad_role_id = ad_role_id;
    }

    public Integer getC_order_id() {
        return c_order_id;
    }

    public void setC_order_id(Integer c_order_id) {
        this.c_order_id = c_order_id;
    }

    public Integer getC_orderline_id() {
        return c_orderline_id;
    }

    public void setC_orderline_id(Integer c_orderline_id) {
        this.c_orderline_id = c_orderline_id;
    }

    public String getXtglfrom() {
        return x_tgl_from;
    }

    public void setXtglfrom(String xtglfrom) {
        this.x_tgl_from = xtglfrom;
    }

    public String getXtglto() {
        return x_tgl_to;
    }

    public void setXtglto(String xtglto) {
        this.x_tgl_to = xtglto;
    }

}
